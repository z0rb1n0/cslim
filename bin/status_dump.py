#!/usr/bin/python3 -uB

import os, sys, re, time
# we need to change our search path to seamlessly include our own modules
SYSTEM_PREFIX = os.path.normpath(os.path.dirname(sys.argv[0]) + "/..")
sys.path.insert(0, SYSTEM_PREFIX + "/lib")


VERSION = 0
PROC_DIR = "/proc"
SYS_DIR = "/sys"
SYS_CPUS_DIR = "%s/devices/system/cpu" % SYS_DIR

import dviz


# Order of fields in /proc/<pid>/stats
PROC_STAT_FIELDS = {name: idx for (idx, name) in enumerate([
	"pid", "comm", "state", "ppid", "pgrp", "session", "tty_nr", "tpgid", "flags",
	"minflt", "cminflt", "majflt", "cmajflt", "utime", "stime", "cutime", "cstime",
	"priority", "nice", "num_threads", "itrealvalue", "starttime", "vsize", "rss",
	"rsslim", "startcode", "endcode", "startstack", "kstkesp", "kstkeip", "signal",
	"blocked", "sigignore", "sigcatch", "wchan", "nswap", "cnswap", "exit_signal",
	"processor", "rt_priority", "policy", "delayacct_blkio_ticks", "guest_time",
	"cguest_time"
])}


def main():


	if (os.getuid() != 0):
		cmd_line = "ssh -o BatchMode=yes root@localhost".split(sep = None)
		cmd_line += [sys.argv[0]]
		print(cmd_line)
		os.execve(
			"/usr/bin/%s" % cmd_line[0],
			cmd_line,
			os.environ
		)


	start_ts = time.time()

	# mounts
	out_json = {
		"processors": dict(),     # 3 level deep dictionary: physical packages, cores, threads
		"processes": dict(),      # indexed by PID
		"mounts": dict()          # indexed by mount ID
	}







	# the CPU core/thread hierarchy must be reassembled by rolling up core/thread siblings
	pp_block = {}
	for cpu_dir in os.listdir(SYS_CPUS_DIR):
		if (not re.search(pattern = r"^cpu\d+$", string = cpu_dir)):
			continue

		cpu_path = "%s/%s" % (SYS_CPUS_DIR, cpu_dir)

		physical_package_id = int(open("%s/topology/physical_package_id" % cpu_path, "r").read())
		thread_id = int(re.sub(pattern = r"[^0-9]", repl = "", string = cpu_dir))
		core_id = int(open("%s/topology/core_id" % cpu_path, "r").read())


		if (physical_package_id not in pp_block):
			pp_block[physical_package_id] = {"cores": {}}

		if (core_id not in pp_block[physical_package_id]["cores"]):
			pp_block[physical_package_id]["cores"][core_id] = {"threads": set()}


		pp_block[physical_package_id]["cores"][core_id]["threads"].add(thread_id)

		out_json["processors"] = {
			"physical_packages": pp_block
		}




	# mounts, initially indexed by ID

	for fields in [lsp.split(sep = None) for lsp in list(filter(
		lambda lfil : len(lfil.strip()),
		open("/proc/1/mountinfo", "rb").read().decode("UTF8").split(sep = "\n")
	))]:

		mount_id = int(fields[0])


		# second scan to attribute children

		out_json["mounts"][mount_id] = {

			"parent_id": int(fields[1]),

			"block_device": tuple(map(int, fields[2].split(sep = ":"))),

			"mount_options": {o.strip() for o in fields[5].split(sep = ",") if o.strip()},
			# create a dictionary for optional fields.
			# We pad the ones without a value with None.
			# Conversion to int is a bit of a gamble, the docs say nothing about types
			"optional_fields": {f.strip():int(v) if v else None for (f,v) in [
				(o.split(sep = ":") + [None])[:2] for o in fields[6:-4]
			]},

			"root": fields[3],

			"mountpoint": fields[4],

			"fs": dict(zip(
					["type", "subtype"],
					(fields[-3].split(sep = ".") + [None])[:2]
			)),

			"source": fields[-2].strip(),

			# see above
			"super_options": {f.strip():v if v else None for (f,v) in [
				(o.split(sep = "=") + [None])[:2] for o in fields[-1].split(sep = ",")
			]},

			# Initial empty list of child mounts.
			# Dereference when copying the skeleton
			"children": set()
		}
		
		

	# post-completion scan to identify the shorthand for children
	for (pm_id, pm_info) in out_json["mounts"].items():
		pm_info["children"].update(filter(
			lambda mi : out_json["mounts"][mi]["parent_id"] == pm_id,
			out_json["mounts"]
		))
		#out_json["mounts"]["by_device"][pm_info[block_dev]] = pm_info



	for proc_entry in os.listdir(PROC_DIR):
		if (not (
				os.path.isdir("%s/%s" % (PROC_DIR, proc_entry))
			and
				re.search(pattern = r"^[0-9]+$", string = proc_entry)
		)):
			continue

		pid = int(proc_entry)
		pid_dir = "%s/%s" % (PROC_DIR, proc_entry)

		proc_block = {
			"pid": pid,
			# Command line as an iterable
			# there is an extra nullchar at the end of the command line
			"cmdline": [arg.decode("UTF8") for arg in open(
				"%s/cmdline" % (pid_dir), "rb"
				).read().split(b"\0")[0:-1]
			],
			"children": set()
		}

		# we split /proc/<pid>/stat into a dictionary.
		# Note that linux does not forbid white spaces nor parentheses
		# in the process image name, which forces us into a contrived split mechanism
		stat_line = open("%s/stat" % pid_dir, "rb").read().decode("UTF8").strip()

		first_bracket = stat_line.index("(")
		last_bracket = stat_line.rindex(")")


		# the character before "(" is a white space
		# Same for the first char after the last bracket
		fields = stat_line[0:first_bracket - 1].split(sep = " ") + [
			stat_line[first_bracket + 1:last_bracket]
		] + stat_line[last_bracket + 2:].split(sep = " ")

		# Everything is converted into integers, except comm and state
		# Some of these are unsigned64.
		# Python switches to arbitrary length arithmetics in case of in overflow,
		# that is slow tho.
		proc_block["stat"] = {
			lp[0]: (str if (lp[0] in ("state", "comm")) else int)(fields[lp[1]])
			for lp in PROC_STAT_FIELDS.items()
		}



		for symlink in ["exe", "cwd"]:
			try:
				proc_block[symlink] = os.readlink("%s/%s" % (pid_dir, symlink))
			except FileNotFoundError as e_noexe:
				exe = None



		out_json["processes"][pid] = proc_block



	sys.stdout.write(dviz.dumps({
		"version": 0,
		"localhost": {
			"timestamp": "%s.%d UTC" % (
				time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime(start_ts)),
				(start_ts % 1) * 1000000000.0
			),
			"data": out_json
		}
	}))
	return 0

if (__name__ == "__main__"):
	sys.exit(main())
