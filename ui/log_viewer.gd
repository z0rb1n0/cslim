extends Label

func _process(_delta: float) -> void:
	self.text = (
			"Current target: %s\n" % [$/root/universe/user/head/eyes/focus.get_collider()]
		+
			"\n".join(logger.get_recent_messages())
	)
