class_name MainMenu
extends TabContainer

const DE_CLASS = preload("res://lib/ui/DataEditor.gd")

const BE_SCHEMA:Dictionary = {
	"id": {
		"label": "Identification string",
		"type": "string",
		"writable": false,
		"modifiers": {
			"min_length": 1,
			"max_length": 256
		}
	},
	"host": {
		"label": "Host",
		"type": "string",
		"writable": true,
		"modifiers": {
			"min_length": 1,
			"max_length": 255,
			"regex": "^[0-9A-Za-z-]{1,63}(\\.[0-9A-Za-z-]{1,63})*$"
		}
	},
	"port": {
		"label": "Port",
		"type": "integer",
		"writable": true,
		"modifiers": {
			"min_value": 1,
			"max_value": 65535
		},
		"default": 443
	},
	"connect_timeout": {
		"label": "Connection Timeout",
		"type": "float",
		"writable": true,
		"modifiers": {
			"min_value": 0.001,
			"max_value": 1800
		},
		"default": 2.0
	},
	"verify_ssl": {
		"label": "Verify host SSL",
		"type": "bool",
		"writable": true
	},
	"username": {
		"label": "User Name",
		"type": "string",
		"writable": true,
		"modifiers": {
			"min_length": 1,
			"max_length": 64
		}
	},
	"password": {
		"label": "Password",
		"type": "string",
		"modifiers": {
			"secret": true,
			"min_length": 0,
			"max_length": 256
		},
		"writable": true
	},
	"info": {
		"type": "string",
		"modifiers": {
			"long": true
		},
		"writable": true
	}
}


func _handle_entries_edit():
	"""
		Handler for entry changes in the tree.
		
		Used exclusively for the "enabled" checkbox.
		
		Unfortunately the signal does not pass any argument so
		we must scan everything
	"""
	var curr_item:TreeItem = $world/backends/scroller/entries.get_root()

	curr_item = curr_item.get_first_child()

	while (curr_item):
		rt_config.backends[curr_item.get_text(0)]["enabled"] = curr_item.is_checked(1)
		curr_item = curr_item.get_next()

	shepherd.update_backends()



func _handle_entries_action(item:TreeItem, column:int, id:int):

	if (column == 2):
		if (id == 0):
			_open_backend_editor(item.get_text(0))
		elif (id == 1):
			_delete_backend(item.get_text(0))
		else:
			logger.bug("Action with index %d was pressed but it's not implemnented" % [id])
	else:
		logger.bug("Button pressed in column #%d. This should never happen" % [id])


func submit_backend(_backend_type:String, backend_id:String, data:Dictionary):
	"""
		Saves/overwrites the backends in the configuration
	"""

	var needs_update:bool = false

	var bes_data:Dictionary = rt_config.backends

	if (backend_id in bes_data):

		# determine whether the backend definition was  changed
		for att_key in data:
			if (att_key == "id"):
				# this does not belong in the stored member
				continue
			if (not (data[att_key] == bes_data[backend_id].get(att_key))):
				needs_update = true
				break
	else:
		needs_update = true

	if (needs_update):
		# Get the name out of the form data, and 
		# recover the configuration as the data does not come with the "enabled" flag
		var new_data:Dictionary = data.duplicate(true)
		if (not new_data.erase("id")):
			push_error("Backend form did not supply a name string")
		new_data["enabled"] = bes_data.get(backend_id, {}).get("enabled", false)
		bes_data[data["id"]] = new_data

		_reload_backends()

	shepherd.update_backends()



func _open_backend_editor(backend_id:String = ""):

	""" Opens a backend editor either for a new backend or to edit an existing one """
	var de:DataEditor = DE_CLASS.new()

	var be_edit = BE_SCHEMA.duplicate(true)

	# new backends must be able to alter the name
	if (not len(backend_id)):
		be_edit["id"]["writable"] = true

	if (len(backend_id)):
		var be_config:Dictionary = rt_config.backends[backend_id]

		# load the form defaults
		for att_key in be_edit:
			var att_val
			if (att_key == "id"):
				# exception as this one is the key itself
				att_val = backend_id
			else:
				att_val = be_config.get(att_key, null)

			if (att_key in be_edit):
				be_edit[att_key]["default"] = att_val

	de.open(self, "Backend", backend_id, be_edit, submit_backend, true)


func _reload_backends():
	"""
		Differentially reloads backends in the tree
	"""
	var tree_backends:Tree = $world/backends/scroller/entries
	var root_node:TreeItem = tree_backends.get_root()

	rt_config.changes.lock()

	var curr_node:TreeItem = root_node.get_first_child()
	var next_node:TreeItem = curr_node

	# first scan cleans up non-existent backends
	while (curr_node):
		next_node = curr_node.get_next()
		if (not (curr_node.get_text(0) in rt_config.backends)):
			curr_node.free()
		curr_node = next_node


	# we create a map with the intended indexes so that we can pop in TreItems in the right place
	var idx_maps:Dictionary = crutch.array_enumerate(
		crutch.dict_keys_sort(rt_config.backends)
	)

	curr_node = root_node.get_first_child()
	for item_idx in idx_maps:
		var backend_id:String = idx_maps[item_idx]

		var be_info = rt_config.backends[backend_id]

		if (curr_node and (curr_node.get_text(0) == backend_id)):
			# backend exists and is in the correct spot, we just update its "enabled" status
			curr_node.set_checked(0, be_info["enabled"])
		elif ((not curr_node) or (curr_node.get_text(0) != backend_id)):
			# There's another backend at this backend's spot, or we're at the end of the list.
			# We insert the new backend
			# new node needed right after the current one

			curr_node = tree_backends.create_item(root_node, item_idx)

			curr_node.set_cell_mode(0, TreeItem.CELL_MODE_STRING)
			curr_node.set_editable(0, false)
			curr_node.set_tooltip_text(0, "Name of the configured backend")
			curr_node.set_text_alignment(0, HORIZONTAL_ALIGNMENT_LEFT)
			curr_node.set_text(0, backend_id)


			curr_node.set_cell_mode(1, TreeItem.CELL_MODE_CHECK)
			curr_node.set_editable(1, true)
			curr_node.set_tooltip_text(1, "???")
			curr_node.set_text_alignment(1, HORIZONTAL_ALIGNMENT_LEFT)
			curr_node.set_checked(1, be_info["enabled"])
			curr_node.set_text(1, "???")


			curr_node.set_editable(2, false)
			curr_node.set_cell_mode(2, TreeItem.CELL_MODE_ICON)
			curr_node.add_button(2, assets.icons["edit"], 0, false, "Edit `%s`" % [backend_id])
			curr_node.add_button(2, assets.icons["delete"], 1, false, "Delete `%s`" % [backend_id])
			curr_node.set_text_alignment(2, HORIZONTAL_ALIGNMENT_LEFT)

		curr_node = curr_node.get_next()

	rt_config.changes.unlock()


func _update_backends_statuses():
	"""
		Accepts the dictionary as passed by shepherd.get_backends_statuses()
		and updates the status texts in the tree
	"""

	var be_statuses:Dictionary = shepherd.get_backends_statuses()
	var root_node:TreeItem = $world/backends/scroller/entries.get_root()

	var curr_node:TreeItem = root_node.get_first_child()
	while (curr_node):
		if (curr_node.is_checked(1)):
			curr_node.set_text(1, be_statuses.get(curr_node.get_text(0), [null, null, "???"])[2])
		else:
			curr_node.set_text(1, "Disabled")
		curr_node = curr_node.get_next()


func _delete_backend(backend_id:String):
	""" Removes a backend from the configuration """
	rt_config.changes.lock()
	if (not rt_config.backends.erase(backend_id)):
		push_error("Backend `%s` was mysteriously gone from the configuration" % [backend_id])
	rt_config.changes.unlock()
	_reload_backends()
	shepherd.update_pool()

func _ready():


	# some objects are initialised from here to save checked script files
	if ($world/backends/add.connect("pressed", _open_backend_editor.bind()) != OK):
		logger.bug("Unable to connect backends tree buttons to handler")

	if ($world/quit.connect("pressed", rt_config.shutdown.bind()) != OK):
		logger.bug("Unable to connect \"QUIT\" button to handler")

	if ($world/backends/scroller/entries.connect("button_clicked", _handle_entries_action.bind()) != OK):
		logger.bug("Unable to connect backends tree to handler (for actions)")

	if ($world/backends/scroller/entries.connect("item_edited", _handle_entries_edit.bind()) != OK):
		logger.bug("Unable to connect backends tree to handler (for edit)")



	var tree_backends:Tree = $world/backends/scroller/entries

	tree_backends.set_hide_root(true)
	tree_backends.set_column_titles_visible(true)
	tree_backends.set_select_mode(Tree.SELECT_ROW)

	tree_backends.set_column_title(0, "Backend")
	tree_backends.set_column_expand(0, true)
	tree_backends.set_column_custom_minimum_width(0, 3)

	tree_backends.set_column_title(1, "Enabled/Status")
	tree_backends.set_column_expand(1, true)
	tree_backends.set_column_custom_minimum_width(1, 3)

	tree_backends.set_column_title(2, "Actions")
	tree_backends.set_column_expand(2, true)
	tree_backends.set_column_custom_minimum_width(2, 1)

	if (not tree_backends.create_item()):
		push_error("Failed to create root node in the backends tree")

	_reload_backends()


var frame_no:int = 0
func _process(_delta):

	var poppables:Array

	if ($world/backends/scroller/entries.is_visible_in_tree()):
		_update_backends_statuses()

	poppables = shepherd.pop()

	var send_s:int
	for msg in poppables:

		logger.debug("got message from backend `%s`: `%s`" % [
			msg["backend_id"], msg["payload"]
		])
		#logger.debug("Responding with \"I got your `%s`\"" % [
			#msg["payload"]
		#])
		#send_s = shepherd.send(msg["backend_id"], ("I got your `%s`" % msg["payload"]).to_utf8_buffer())

	if (false and frame_no and (not (frame_no % 100))):
		var send_buf:PackedByteArray = ("Frame %20d" % frame_no).to_utf8_buffer()
		send_buf = "a".to_utf8_buffer()

		send_s = shepherd.send("local", send_buf)
		logger.debug("Sent frame message with result %d" % [send_s])

	frame_no += 1
