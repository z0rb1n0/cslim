class_name ProcessingNode
extends CSLIMNode
#func get_class(): return "ProcessingNode"

func _init():
	super._init()
	type_label = "Node"
	show_label = true

# override as we manually update our static children
func update(data:Dictionary):

	super.update(data)

	$processor_pps.update(_data["data"]["processors"]["physical_packages"])
	$memory.update(_data["data"]["memory"])
