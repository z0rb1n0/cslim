class_name ProcessorPhysicalPackagesSet
extends CSLIMNode
#func get_class(): return "ProcessorPhysicalPackagesSet"

const _ct:PackedScene = preload("res://entities/processor_pp/processor_pp.tscn")

func _init():
	super._init()
	child_template = _ct
	children_holder = "."
	children_path = []
	children_margin = Vector3.ONE * 0.1
	children_spacing = Vector3.ONE * 0.05
	max_children_per_row = 256
	max_rows_per_layer = 1048576
	show_label = false
