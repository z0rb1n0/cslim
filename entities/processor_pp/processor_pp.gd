class_name ProcessorPhysicalPackage
extends CSLIMNode
#func get_class(): return "ProcessorPhysicalPackage"

const _ct:PackedScene = preload("res://entities/processor_core/processor_core.tscn")

func _init():
	super._init()
	type_label = "Processor Package"
	child_template = _ct
	children_holder = "processor_cores"
	children_path = ["cores"]
	children_margin = Vector3.ONE * 0.1
	children_spacing = 0.5 * children_margin
	show_label = true
	max_children_per_row = 1048576
	max_rows_per_layer = 1048576



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
