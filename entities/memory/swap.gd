class_name SwapSpace
extends CSLIMNode
#func get_class(): return "SwapSpace"

const _ct:PackedScene = preload("res://entities/memory_chunk/memory_chunk.tscn")

func _init():
	super._init()
	children_holder = "."
	children_path = ["chunks"]
	child_template = _ct
	
	children_margin = Vector3.ONE * 0.02
	children_spacing = Vector3.ONE * 0.01

	show_label = true
	type_label = "Swap Space"
	label_name = "\b"


func update(data):

	# re-define the square if things change
	max_children_per_row = int(ceil(sqrt(len(data["chunks"]))))
	max_rows_per_layer = max_children_per_row

	extra_info = "Total: %.3f MB\n" % (float(data["total"]) / 1048576.0)
	extra_info += "Chunk size: %.3f MB\n" % (data["chunk_size"] / 1048576.0)
	extra_info += "Total chunks: %d" % (len(data["chunks"]))
	super.update(data)
