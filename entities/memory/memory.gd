extends CSLIMNode
class_name Memory
#func get_class(): return "Memory"


func _init():
	super._init()
	show_label = true
	label_name = "\b"
	children_margin = Vector3.ONE * 0.02
	children_spacing = Vector3.ONE * 0.01



func update(data):
	super.update(data)

	var usable_width:float = 1 - (2 * children_margin.x) - children_spacing.x
	# We re-scale and re-center RAM and swap based checked their ratio
	var ram_fraction:float = data["ram"]["total"] / (data["ram"]["total"] + data["swap"]["total"])

	# width of RAM is its fraction of free space - margins
	$ram.designated_scale = Vector3(
		usable_width * ram_fraction,
		$ram.scale.y,
		$ram.scale.z
	)

	$ram.designated_translation = Vector3(
		-0.5 + children_margin.x + 0.5 * $ram.designated_scale.x,
		$ram.position.y,
		$ram.position.z
	)

	$ram.update(data["ram"])

	if (ram_fraction < 1):
	
		$swap.designated_scale = Vector3(
			usable_width * (1- ram_fraction),
			$swap.scale.y,
			$swap.scale.z
		)

		$swap.designated_translation = Vector3(
			$ram.designated_translation.x + (0.5 * $ram.designated_scale.x) + children_spacing.x + 0.5 * $swap.designated_scale.x,
			$swap.position.y,
			$swap.position.z
		)
		$swap.visible = true
		$swap.update(data["swap"])

	else:
		# there is no swap.
		# We don't just make it invisible as it might have
		# children. We shrink it into nothing
		$swap.designated_scale = Vector3.ZERO
