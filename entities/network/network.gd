class_name Network
extends CSLIMNode
#func get_class(): return "Network"


func _init():
	super._init()
	child_template = load("res://entities/processing_node/processing_node.tscn")
	children_holder = "processing_nodes"
	children_path = ["nodes"]
	children_margin = Vector3.ONE * 0.2
	children_spacing = Vector3.ONE * 0.1
