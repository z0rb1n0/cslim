class_name CSLIMUser
extends CharacterBody3D


# When pivoting, we look for an intersection point
# between the line of sight and any of two imaginary planes
# below or above the user by MIN_PIVOT_DISTANCE.
# If there no intersection or that is too far,
# we limit that to MAX_PIVOT_DISTANCE
const MIN_PIVOT_DISTANCE:float = 2.0
const MAX_PIVOT_DISTANCE:float = 20.0

var pivot_point
var pivot_plane:Plane
var user_on_pplane:Vector3
var p_momentum:Vector3 = Vector3.ZERO
var p_moi:Quaternion = Quaternion.IDENTITY # moment of inertia


@onready var universe:Node3D = $/root/universe
@onready var circling_pivot:Node3D = $/root/universe/circling_pivot
@onready var a_pod:StaticBody3D = $active_pod
@onready var menu:Control = $/root/universe/hud/main_menu
@onready var head:Node3D = $head
@onready var eyes:Camera3D = self.head.get_node("eyes")
@onready var ray_focus:RayCast3D = self.eyes.get_node("focus") # either crosshair or mouse pointer

func _ready():

	menu.visible = true

	ray_focus.add_exception(a_pod)

	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _physics_process(_delta: float) -> void:

	# the position of the mouse ray is always calculated, as it doubles as crosshair when captured
	var mp := self.get_viewport().get_mouse_position()
	self.ray_focus.global_transform.origin = self.eyes.project_ray_origin(mp)
	self.ray_focus.global_transform.basis = Basis(self.eyes.project_position(mp, 1.0).normalized(), 0)
	self.ray_focus.target_position = self.eyes.project_ray_normal(mp) * self.eyes.far


func _input(ev:InputEvent):


	# if we're not in the menu, handle looking & pivoting
	if (! menu.visible):

		# free/lock mouse.
		if (Input.is_action_just_pressed("release_commands")):
			Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		elif (Input.is_action_just_released("release_commands")):
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

		# handle mouse effects on player
		if (Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED):
			if (ev is InputEventMouseMotion):

				var mouse_motion = Vector2(
						(ev.screen_relative / self.get_viewport().get_visible_rect().size)
					*
						2 * PI
					*
						rt_config.settings["controls"]["look_sensitivity"]
				)

				if (typeof(pivot_point) != TYPE_NIL):

					# Pivoting must not allow for rolling,
					# so we only move the player keep the eyes checked
					# the pivot

					# move player. Camera3D is a child and will follow
					# Don't move if up we're too close to the poles
					var m_y:float = 0
					var u_angle:float = circling_pivot.global_transform.basis.y.abs().angle_to(
						(global_transform.origin - circling_pivot.global_transform.origin).normalized()
					) - 0.5 * PI

					if (
							(abs(u_angle) < (PI * 0.45))
						or
							(sign(mouse_motion.y) == sign(u_angle))
					):
							m_y = -mouse_motion.y

					self.global_transform.origin = Transform3D(
						global_transform.basis,
						global_transform.origin - circling_pivot.global_transform.origin
					).rotated(
						global_transform.basis.y,
						mouse_motion.x
					).rotated(
						global_transform.basis.x,
						m_y
					).origin

					# move back
					self.global_transform.origin += circling_pivot.global_transform.origin

					user_on_pplane = pivot_plane.project(global_transform.origin)

					# rotate the user to face the projection of the pivot at its eye level
					self.look_at(
						circling_pivot.global_transform.origin + (global_transform.origin - user_on_pplane),
						global_transform.basis.y
					)

					# Totate the camera to face the pivot. The camera should never look left/right,
					# but given that the user is already aligned that will add up anyway
					self.head.look_at(
						circling_pivot.global_transform.origin,
						self.global_transform.basis.y
					)

				else:

					# player is looking around. left-right rotations rotate
					# the whole player, up-down only the camera

					if (Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED):
						# rotate the player
						self.rotate(self.transform.basis.y, -mouse_motion.x)
						self.head.rotate(head.transform.basis.x, -mouse_motion.y)


	# In menu, the mouse is free-floating.
	if (Input.is_action_just_pressed("ui_escape")):

		menu.visible = (! menu.visible)
		if (menu.visible):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):

	var p_impulse:Vector3 = Vector3.ZERO
	var p_torque:Quaternion = Quaternion.IDENTITY

	# movement smoothing is done by adding to a "momentum" vector

	# Velocity and moment of inertia decay by 3% per ms
	self.p_momentum *= float(max(1 - (0.03 * sqrt(delta * 1000)), 0))
	self.p_moi = p_moi.slerp(Quaternion.IDENTITY, 0.03 * sqrt(delta * 1000))

	# we stop feeding impulses if the buttons are released
	# but still coast some due to remaining momentum
	p_impulse = Vector3.ZERO
	p_torque = Quaternion.IDENTITY

	# no motion is allowed in pivot mode, but we still coast
	if (Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED):
		# Forward/backward & up/down & roll from the camera
		# (so that we {a|de}scend the direction we're looking at).
		# Left/right  from the player
		if (Input.is_action_pressed("impulse_forward")):
			p_impulse += Vector3.FORWARD
		if (Input.is_action_pressed("impulse_backward")):
			p_impulse += Vector3.BACK
		if (Input.is_action_pressed("impulse_down")):
			p_impulse += Vector3.DOWN
		if (Input.is_action_pressed("impulse_up")):
			p_impulse += Vector3.UP
		if (Input.is_action_pressed("impulse_left")):
			p_impulse += Vector3.LEFT
		if (Input.is_action_pressed("impulse_right")):
			p_impulse += Vector3.RIGHT
		if (Input.is_action_pressed("roll_left")):
			p_torque = Quaternion(Vector3.FORWARD, -0.02)
		if (Input.is_action_pressed("roll_right")):
			p_torque = Quaternion(Vector3.FORWARD, 0.02)

	p_impulse = p_impulse.normalized() * 0.01 * rt_config.settings["controls"]["impulse_multiplier"]
	p_torque = p_torque.normalized()
	
	#$/root/universe/main_overlay/vector_debug.add_vector(self.global_transform.origin, p_torque.get_axis())

	#p_torque = p_torque.normalized() * rt_config.settings["controls"]["roll_multiplier"]

	# If we're inside a CSLIMNode, we downscale own impulse
	# based checked the smallest dimension in the global scale of that object (still capped at 1)
	if ((p_impulse.length() > 0.000001) and is_instance_valid(universe.ddn_root)):
		var csnp:String = universe.ddn_root.get_point_container(self.global_transform.origin)
		if (len(csnp)):
			var csn:CSLIMNode = get_node(csnp)
			if (is_instance_valid(csn)):
				var csn_s:Vector3 = csn.global_transform.basis.get_scale()
				p_impulse *= (
						CSLIMNode.SCALE_TO_IMPULSE_LIMIT
					*
						abs(csn_s[csn_s.min_axis_index()])
				)
				#print("IN: `%s`(%s)" % [csnp, p_impulse])

	# moment of inertia is independent of proximity to objects

	p_momentum += p_impulse
	p_moi = p_moi.slerp(p_torque, rt_config.settings["controls"]["roll_multiplier"])

	if p_torque.get_angle():
		self.rotate(p_torque.get_axis(), p_torque.get_angle())

	# significant movement forces pivot point recalculation
	if (p_momentum.length() > 0.001):

		#print("moving: %d, %.6f" % [Time.get_ticks_msec(), p_impulse.length()])
		pivot_point = null
		#self.global_transform *= Transform3D(Basis(p_moi), p_momentum)
		#self.global_transform *= Transform3D(Basis(Basis.IDENTITY), p_momentum)
		self.translate(p_momentum)

	else:

		# We're still checking if orbital view is engaged,
		# but NOT if the cursor isn't captured. That resets everything
		if (
				Input.is_action_pressed("camera_orbit")
			and
				(Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED)
		):

			# if the user is stationary, they can set the pivot
			if (
					(p_momentum.length() < 0.001)
				and
					(typeof(pivot_point) == TYPE_NIL)
			):

				# If this is the first frame at which they're holding the button,
				# we pick the pivot

				# Intercepted plane is "below" the user?
				pivot_point = Plane(
					global_transform.origin - global_transform.basis.y * MIN_PIVOT_DISTANCE,
					global_transform.origin - global_transform.basis.y * MIN_PIVOT_DISTANCE + global_transform.basis.x,
					global_transform.origin - global_transform.basis.y * MIN_PIVOT_DISTANCE + global_transform.basis.z
				).normalized().intersects_segment(
					head.global_transform.origin,
					head.global_transform.origin + MAX_PIVOT_DISTANCE * -head.global_transform.basis.z.normalized()
				)
				# above maybe?
				if (not pivot_point):
					pivot_point = Plane(
						global_transform.origin + global_transform.basis.y * MIN_PIVOT_DISTANCE,
						global_transform.origin + global_transform.basis.y * MIN_PIVOT_DISTANCE + global_transform.basis.x,
						global_transform.origin + global_transform.basis.y * MIN_PIVOT_DISTANCE + global_transform.basis.z
					).normalized().intersects_segment(
						head.global_transform.origin,
						head.global_transform.origin + MAX_PIVOT_DISTANCE * -head.global_transform.basis.z.normalized()
					)
				if (typeof(pivot_point) == TYPE_NIL):
					# we default to MAX_PIVOT_DISTANCE ahead of the camera
					pivot_point = (
							head.global_transform.origin
						+
							-head.global_transform.basis.z.normalized() * MAX_PIVOT_DISTANCE
					)


				circling_pivot.global_transform.origin = pivot_point
			
				# The circling pivot always starts aligned to the user
				circling_pivot.global_transform.basis = global_transform.basis

				# we need a plane to calculate constrained rotation for the user
				pivot_plane = Plane(
					circling_pivot.global_transform.origin,
					circling_pivot.global_transform.origin + circling_pivot.global_transform.basis.x,
					circling_pivot.global_transform.origin + circling_pivot.global_transform.basis.z
				).normalized()

				# When first displayed, the pivoting grid is scaled to fill distance to user
				user_on_pplane = pivot_plane.project(global_transform.origin)
	
				circling_pivot.scale = (
					Vector3(
						(circling_pivot.global_transform.origin - user_on_pplane).length(),
						# we sort vertical scaling later
						0,
						(circling_pivot.global_transform.origin - user_on_pplane).length()
					)
				)


		else:
			# release rotation mode
			pivot_point = null


	circling_pivot.visible = typeof(pivot_point) != TYPE_NIL
	if (circling_pivot.visible):
		# We need to keep vertically scaling the pivoting grid.
		# Vertical basis of the grid is the inverted projection vector of the user checked the plane.
		# That correctly inverts the grid backfaces when it's viewed for below
		circling_pivot.global_transform.basis.y = (global_transform.origin - user_on_pplane)

	self.transform = self.transform.orthonormalized()
