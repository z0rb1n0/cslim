class_name CSLIMBackend
extends Node

enum BackendStatus {
	STATUS_INACTIVE = 0,
	STATUS_CONNECTING = 1,
	STATUS_ERROR = 2,
	STATUS_CONNECTED = 3
}

enum BackendMode {
	MODE_DIRECT = 0,
	MODE_SSH_TUNNEL = 1
}


const URL_SCHEME:String = "wss"
const URL_PATH:String = "control" # omit the first slash
const WEBSOCKET_PROTOCOL:String = "cslim0"

# timers are in seconds
const POLL_INTERVAL:float = 0.1
const BACKOFF_AVERAGE:float = 5.0
const BACKOFF_VARIANCE:float = 0.2 # a fraction of BACKOFF_AVERAGE

var _url:String = ""
var _connect_timeout:float = 3.0
var _mode:int = BackendMode.MODE_DIRECT
var _username:String = ""
var _password:String = "" # unfortunately we need to store it for subsequent calls
var _verify_ssl:bool = true
var _ssh_pkey:String = ""
var _active:bool = false # current state of the backend
var _status:BackendStatus = BackendStatus.STATUS_INACTIVE
var _last_error:int = -1

# The config hash is used to compare running backends with their
# intended configuration
var _conf_hash:String = ""

var _websocket:WebSocketPeer = WebSocketPeer.new()
var _thread:Thread = Thread.new()
var _inbox:Array = []
var _lock:Mutex = Mutex.new()

func _block_set(_value):
	push_error("Read-only property")
	assert(false)

var url:String:
	get: return _url
	set(value): return _block_set(value)

var connect_timeout:float:
	get: return _connect_timeout
	set(value):	return _block_set(value)

var mode:int:
	get: return _mode
	set(value): _block_set(value)

var username:String:
	get: return _username
	set(value): return _block_set(value)

var password:String:
	get: return "********" # :-)
	set(value):
		return _block_set(value)

var verify_ssl:bool:
	get: return _verify_ssl
	set(value): return _block_set(value)

var ssh_pkey:String:
	get: return _ssh_pkey
	set(value): return _block_set(value)

var conf_hash:String:
	get: return _conf_hash
	set(value): return _block_set(value)

var active:bool = false:
	get: return _active
	set(value):	return _set_activation_state(value)

var status:int:
	get: return _status
	set(value): return _block_set(value)

var last_error:int:
	get: return _last_error
	set(value): return _block_set(value)

var inbox:Array = []:
	get: return _inbox
	set(value): return _block_set(value)

var lock:Mutex:
	get: return _lock
	set(value): return _block_set(value)


static func hash_config(
		c_host:String,
		c_port:int = 443,
		c_connect_timeout:float = 3.0,
		c_mode:int = BackendMode.MODE_DIRECT,
		c_username:String = "",
		c_password:String = "",
		c_verify_ssl:bool = true,
		c_ssh_pkey:String = ""
):
	# Creates an hash of a call signature to compare configurations between backends"""
	return String("%s\t%d\t%.9f\t%d\t%s\t%s\t%d\t%s" % [
		c_host, c_port, c_connect_timeout, c_mode,
		c_username, c_password, int(c_verify_ssl), c_ssh_pkey
	]).sha1_text()

func _init(
		c_host:String,
		c_port:int = 443,
		c_connect_timeout:float = 3.0,
		c_mode:int = BackendMode.MODE_DIRECT,
		c_username:String = "",
		c_password:String = "",
		c_verify_ssl:bool = true,
		c_ssh_pkey:String = ""
):

	_url = "%s://%s:%d/%s" % [URL_SCHEME, c_host, c_port, URL_PATH]
	_connect_timeout = c_connect_timeout
	_mode = c_mode
	_username = c_username
	_password = c_password
	_verify_ssl = c_verify_ssl
	_ssh_pkey = c_ssh_pkey

	_conf_hash = hash_config(
		c_host, c_port, c_connect_timeout, c_mode,
		c_username, c_password, c_verify_ssl, c_ssh_pkey
	)


	# Here are the signals if we want to implement those instead
	#_websocket.connect("connection_closed",Callable(self,"_closed"))
	#_websocket.connect("connection_error",Callable(self,"_closed"))
	#_websocket.connect("connection_established",Callable(self,"_connected"))
	#_websocket.connect("data_received",Callable(self,"_on_data"))

func _set_activation_state(target_state:bool):
	if (target_state):
		start()
	else:
		stop()

func start():
	if (not active):
		logger.debug("Starting backend loop for `%s`" % [url])
		_active = true
		if (_thread.start(self._main_loop.bind()) != OK):
			_active = false
			logger.error("Could not start backend thread for `%s`. Aborted" % [url])

func stop():
	if (active):
		logger.debug("Stopping backend loop for `%s`" % [url])
		_active = false
		_thread.wait_to_finish()

func _ws_is_connected() -> bool:
	#
	# Websocket status checking is a bit confusing; 2 methods seem
	# to be relevant.
	# This function exists to easy change testing semantics as we
	# learn more
	#
	var ready_state:WebSocketPeer.State = _websocket.get_ready_state()
	return (ready_state == WebSocketPeer.STATE_OPEN)

func _ws_is_connecting() -> bool:
	#
	# Websocket status checking is a bit confusing; 2 methods seem
	# to be relevant.
	# This function exists to easy change testing semantics as we
	# learn more
	#
	var ready_state:WebSocketPeer.State = _websocket.get_ready_state()
	return (ready_state == WebSocketPeer.STATE_CONNECTING)

func _get_backoff_time() -> int:
	#
	# Returns a randomised backoff time according to the parameters,
	# in microseconds
	#

	var r_gen:RandomNumberGenerator = RandomNumberGenerator.new()
	r_gen.randomize()
	var var_range:float = (BACKOFF_AVERAGE * BACKOFF_VARIANCE)
	var bo_time:float = r_gen.randf_range(
			BACKOFF_AVERAGE - 0.5 * var_range,
			BACKOFF_AVERAGE + 0.5 * var_range
	)
	return int(round(1000000.0 * bo_time))


func _main_loop():

	var backoff_end:int = 0
	var deadline:int = -1

	while (true):

		_lock.lock()

		var loop_ts:int = Time.get_ticks_usec()

		if (_ws_is_connected() or _ws_is_connecting()):
			_websocket.poll()

		# if we're connected but the backend was disabled, we disconnect and shut down
		if (_active):

			if (_ws_is_connected()):

				_status = BackendStatus.STATUS_CONNECTED

				if (loop_ts < deadline):
					deadline = -1
					logger.info("Connection to `%s` successfully established" % [url])

				# ready for data
				# here's where we actually handle inbox and outbox
				var new_messages:int = _websocket.get_available_packet_count()

				if (new_messages):
					# We mark the time of delivery.
					# FIXME: this is wildly inaccurate & jittery due to the polling cycle
					var timestamp_r:float = Time.get_unix_time_from_system()

					logger.debug("Received %d new messages. Moving to inbox" % [new_messages])
					_lock.lock()
					for _msg_id in range(new_messages):

						# try to parse the received message.
						# Check the docstring for pop() for the internal dictionary format

						var jmsg_in = JSON.new()
						var j_err:Error = jmsg_in.parse(_websocket.get_packet().get_string_from_utf8())
						if (j_err != OK):
							logger.error("Unable to parse JSON message from backend (line #%d, `%s`)" % [
								jmsg_in.get_error_line(), jmsg_in.get_error_message()
							])
							continue

						var jr_res = jmsg_in.data
						if (typeof(jr_res) != TYPE_DICTIONARY):
							logger.error("Backend JSON message did not decode into a dictionary (type: %d)" % [typeof(jr_res)])
							continue

						if (jr_res.size() != 3):
							logger.error("The number of attributes in the JSON object from the backend is different than 3 (%d: %s)" % [
								jr_res.size(), jr_res.keys
							])

						if (not jr_res.has_all(["type", "timestamp", "payload"])):
							logger.error("Incorrect attributes in JSON object from the backend: %s " % [jr_res.keys()])

						# all seems good. Time to re-write a couple members in the dictionary a bit before we append to the list
						jr_res["timestamp_r"] = timestamp_r
						jr_res["timestamp_g"] = crutch.iso8601_to_unix(jr_res["timestamp"])
						if (not jr_res.erase("timestamp")):
							logger.error("Timestamp erase failed. Odd...")

						_inbox.append(jr_res)

					_lock.unlock()

			else:

				# not connected. Might be in backoff time, still connecting or just
				# ripe for next connection

				if (backoff_end >= loop_ts):

					_status = BackendStatus.STATUS_ERROR
					#logger.debug("Waiting for backoff time before re-attempting connection to `%s` (%.3f seconds left)" % [
					#	_url,
					#	0.000001 * float(backoff_end - loop_ts)
					#])

				else:

					if (_ws_is_connecting()):

						if (loop_ts < deadline):

							logger.debug("Connection to `%s` is not ready. Will keep polling until timeout (%.3f seconds left)" % [
								_url,
								0.000001 * float(deadline - loop_ts)
							])

						else:

							_last_error = ERR_TIMEOUT
							_status = BackendStatus.STATUS_ERROR
							var backoff_time:int = _get_backoff_time()
							backoff_end = loop_ts + backoff_time
							logger.debug("Connection to `%s` has timed out. Will wait for %.3f seconds" % [
								_url,
								0.000001 * float(backoff_time)
							])

					else:

						if (loop_ts < deadline):

							# had a failure before deadline...
							deadline = -1
							_last_error = ERR_CANT_CONNECT
							_status = BackendStatus.STATUS_ERROR
							var backoff_time:int = _get_backoff_time()
							backoff_end = loop_ts + backoff_time
							logger.error("Connection to `%s` has failed. Will back off for %.3f seconds" % [
								_url, 0.000001 * float(backoff_time)
							])

						else:

							_status = BackendStatus.STATUS_CONNECTING
							# must connect first...
							logger.debug("Backend enabled, but peer is not connected/connecting. Attempting connection to %s (timeout is %.3f seconds)" % [
								_url, _connect_timeout
							])

							var r_headers:PackedStringArray = []
							if (len(_username) or len(_password)):
								r_headers.append("Authorization: Basic %s" % [
									Marshalls.utf8_to_base64("%s:%s" % [
										_username,
										_password
									])
								])

							#_websocket.verify_ssl = verify_ssl
							_websocket.handshake_headers = r_headers
							_websocket.supported_protocols = [WEBSOCKET_PROTOCOL]

							var tls_options:TLSOptions = TLSOptions.client() if self._verify_ssl else TLSOptions.client_unsafe()

							deadline = loop_ts + int(round(1000000.0 * _connect_timeout))
							_last_error = _websocket.connect_to_url(_url, tls_options)

							if (_last_error):
								var backoff_time:int = _get_backoff_time()
								backoff_end = loop_ts + backoff_time
								logger.error("Connection to `%s` has failed (code %d). Will back off for %.3f seconds" % [
									url, last_error, backoff_time
								])

		else:

			# time to shut down
			if (_ws_is_connected()):
				# need a disconnect here
				logger.debug("Backend is disabled, but still connected to `%s`. Disconnecting" % [url])
				_websocket.close(1000, "goodbye")
			else:
				# deactivted and disconnected. we're done
				_lock.unlock()
				break

		_lock.unlock()
		OS.delay_usec(int(round(1000000.0 * POLL_INTERVAL)))


func pop(max_messages:int = -1) -> Array:
	#
	# Pops CSLIM messages from the inbox
	#
	#
	# Args:
	# max_messages:	How many messages to pop, at most

	# Returns:
	# 	an array containing the popped messages. Can be empty
	# 	Each message is a dictionary comprised of 3 members:
	#		"type":  			(string)Type of the message
	#		"timestamp_g":	(float)POSIX timestamp of the message at the moment it was generated in the backend
	#		"timestamp_r":	(float)POSIX timestamp of the message at the moment it was received (+/- polling interval)
	#		"payload":		(variant)The decoded payload field (from JSON).
	#

	var a_out:Array = []
	_lock.lock()
	while (len(inbox) and ((max_messages < 0) or (len(a_out) <= max_messages))):
		a_out.append(inbox.pop_front())
	_lock.unlock()
	return a_out


func send(message:PackedByteArray) -> int:
	#
	# Simply sends the message, assuming that the backend is connected
	#
	# Args:
	# 	message:	The raw message to be sent
	#
	# Returns:
	# 	An error code
	#

	var i_out:int = ERR_BUG
	_lock.lock()
	if (active and _ws_is_connected()):
		i_out = _websocket.put_packet(message)
	else:
		logger.debug("Backend is inactive and/or disconnected from to `%s`" % [url])
		i_out = ERR_UNCONFIGURED
	_lock.unlock()
	return i_out
