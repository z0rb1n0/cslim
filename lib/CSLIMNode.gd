class_name CSLIMNode
extends Node3D
#@warning_ignore("native_method_override")
#func get_class(): return "CSLIMNode"



# Ratio between global scale of the CSLIMOBject in which a player is
# and the maximum impulse that can be achieved
const SCALE_TO_IMPULSE_LIMIT = 0.1

const DEBUG_MODE:bool = true

const SIGN_SCENE:PackedScene = preload("res://entities/sign.tscn")

# CSLIM-specific label. Defaults to the class name
var type_label:String = ""

# String to override the object name in the label.
# If empty the object name is used.
# A single backspace in the string ("\b") suppresses
# the object name altogether
var label_name:String = ""


# extra info. Will be added to the label if present
var extra_info:String = ""

# Whether or not a node should show its label when rendered
var show_label:bool = false

# Relative path to the object where auto-instantiated children reside.
# Set to  "." to instantiate children directly under the node itself
# Set to "" to disable recursion
# Override to your liking in your _init() override.
var children_holder:String = ""

# Path3D in the dictionary where the instantiatable keys reside
# The empty array uses the root of the dictionary
# Override to your liking in your _init() override
var children_path:Array = ["things", "subthings"]



# The position this node will progressively migrate to
# Set to infinity to disable auto-position
var designated_translation:Vector3 = crutch.GDBugVector3INF

# The scale this node will progressively adapt.
# Set to infinity to disable auto-scale
var designated_scale:Vector3 = crutch.GDBugVector3INF


# The packed scene you want to instantiate
var child_template:PackedScene = null
# It is recommended that you assign a preloaded class
# member to this to avoid reloading. EG:
# var _ct:PackedScene = preload("res://whatever.tscn")
# # then in _init()
# child_template = _ct



# how many children objects to distribute in each left-to-right row
var max_children_per_row = 4

# how many rows of children to layout for each layer (back-to-front)
var max_rows_per_layer = 16


# Margin of children grid from this object
# (in this objects' local coordinates scaling)
var children_margin:Vector3 = Vector3(0.2, 0.2, 0.2)

# spacing between cildren
# (in this objects' local coordinates scaling)
var children_spacing:Vector3 = Vector3(0.1, 0.1, 0.1)


# Whether or not to constraint the aspect ratio of the children
# (they will be scaled down even in dimensions where there is room to spare)
var children_ar_lock:bool = true




# Dict of data-driven children, which allows us to
# delete disappearing data nodes whilst leaving everthing else
# alone
var _ddcs:Dictionary = {}

# position momentum
var _momentum_t:Vector3 = Vector3.ZERO

# scale momentum
var _momentum_s:Vector3 = Vector3.ZERO

var _data:Dictionary
var data:Dictionary :
	get:
		return self._data
	set(new_data):
		self._data = new_data



func get_point_container(point:Vector3):
	"""
		Recurses into all the children to find which one
		contains the passed point (in global space).

		If that fails, attempts it against itself

		Returns the path of the container,
		or the empty string in case of no match
	"""
	var c_path:String = ""

	for child in self.get_children():
		if (not child.has_method("get_point_container")):
			continue
		c_path = child.get_point_container(point)
		if (len(c_path)):
			# done
			return c_path

	# recursion found noting. Fall back to ourselves
	var box:AABB = AABB(
		# Apparently the "inside" of an AABB is considered
		# "front-to-back", so we do not use crutch.Vector3Positive
		self.to_global(transform.origin - (0.5 * Vector3.ONE)),
		global_transform.basis.x + global_transform.basis.y + global_transform.basis.z
	)

	if (box.has_point(point)):
		c_path = self.get_path()

	return c_path


func get_label_text():

	var l:String = type_label if (len(type_label)) else self.get_class()

	if (label_name != "\b"):
		l += ":\n"
		l += "\n"
		l += label_name if len(label_name) else String(self.name)

	if (len(extra_info)):
		l += "\n"
		l += "\n"
		l += "\n"
		l += "--------------------------------\n"
		l += "%s\n" % extra_info
		l += "--------------------------------\n"

	return l


# example _init(), for you to override
func _init() -> void:


	#super().init() # https://github.com/godotengine/godot/issues/84850
	#children_holder = ""
	#child_template = load("res://entities/foobar.tscn")
	#children_holder = "processor_pps"
	#children_path = ["things"]
	pass



func _ready():


	# Add sign if there's need for a label
	if (show_label and is_instance_valid(SIGN_SCENE)):
		var c_sign = SIGN_SCENE.instantiate()
		c_sign.visible = false
		c_sign.name = "_cslim_sign"
		c_sign.get_node("vp/text").text = get_label_text()

		add_child(c_sign)

#		c_sign.global_transform = Transform3D(
#			Vector3.ONE * c_sign.global_transform.basis.get_scale().max_axis(),
#			c_sign.global_transform.origin
#		)
		# sticks out just a little to avoid z-fight
		c_sign.rotation = Vector3(PI * 0.5, 0, 0)
		c_sign.position = Vector3.BACK * 0.505
		c_sign.visible = true

		#logger.debug("SIGN: %s/%s (%s)" % [get_path(), show_label, get_class()])


	# create debug cage if enabled
	if (DEBUG_MODE):

		var debug_cage = MeshInstance3D.new()
		debug_cage.name = "debug_cage"
		debug_cage.mesh = load("res://meshes/debug_axes.obj")
		var cage_material:Material
		if (self.has_method("get_surface_override_material")):
			cage_material = get_node(".").get_surface_override_material(0)

		if (is_instance_valid(cage_material)):
			cage_material = cage_material.duplicate()
		else:
			cage_material = StandardMaterial3D.new()

		cage_material.flags_unshaded = true
		cage_material.params_cull_mode = StandardMaterial3D.CULL_DISABLED

		debug_cage.set_surface_override_material(0, cage_material)
		debug_cage.position = -crutch.Vector3Positive * 0.51
		debug_cage.scale = Vector3.ONE * 1.02
		add_child(debug_cage)

	visible = true



func update(new_data:Dictionary):
	self._data = new_data

	var sn:Label = get_node_or_null("_cslim_sign/vp/text")
	if (is_instance_valid(sn)):
		var l:String = get_label_text()
		logger.debug("Label for `%s` is `%s`" % [
			get_path(),
			l.replace("\n", "\\n")
		])
		sn.text = l

	if (not children_holder.length()):
		#logger.debug("Children holder not specified for `%s(%s)`. Stopping recursion" % [
			#get_class(), name
		#])
		return null


	if (typeof(self._data) != TYPE_DICTIONARY):
		logger.error("Data set is not a dictionary!!!. Cannot continue")
		return null

	# if the node is ourselves, we don't bother resolving
	var c_nest:Node
	if (children_holder == "."):
		c_nest = self
	else:
		c_nest = get_node_or_null(children_holder)
		if (not c_nest):
			logger.error("Node `%s` does not exist. Cannot continue" % children_holder)
			return null

	var data_children = crutch.dict_path(self._data, children_path)

	if (typeof(data_children) == TYPE_NIL):
		logger.error("Could not resolve path `%s` through data dictionary. Cannot continue" % str(children_path))
		return null

	

	if (typeof(data_children) != TYPE_DICTIONARY):
		# If the data is an array, what the dictionary is turned into depends checked the
		# type of the members. For arrays of scalars, the scalar itself is turned
		# into a key (converted as string) and each member contains the empty array.
		# For any other case, the index in the array determines the key and the content
		# is left as-is.
		
		# This logic can be very confusing for mixed-type arrays,
		# so we throw an error if any is encountered

		if (typeof(data_children) == TYPE_ARRAY):
			var dt:Dictionary = {}
			var met_scalars:bool = false
			for i in range(data_children.size()):
				if (typeof(data_children[i]) in [TYPE_INT, TYPE_FLOAT, TYPE_STRING]):
					met_scalars = true
					dt[str(data_children[i])] = {}
				else:
					if (met_scalars):
						logger.error("%s:%s's children_path contains both scalars and nonscalars. Aborting" % [
							get_class(), get_path()
						])
					dt[str(i)] = data_children[i]
			data_children = dt
		else:
			logger.error("Data at `%s` is not a dictionary or cannot be converted to one. Cannot continue" % children_path)
			return null

	# First delete children if they don't exist
	# We can't touch the dictionary while we scroll it, and duplicating the whole thing
	# is expensive checked large sets
	var dead_ddcs:Array = []
	for old_c in _ddcs:
		if (not (old_c in data_children)):
			if (is_instance_valid(_ddcs[old_c])):
				dead_ddcs.append(old_c)

	for old_c in dead_ddcs:
		var obj_full_path:String = "%s/%s/%s" % [get_path(), children_holder, old_c]
		if (DEBUG_MODE):
			logger.debug("Object mapped to `%s` disappeared from the data set. Removing nodes" % [
				obj_full_path
			])
			_ddcs[old_c].queue_free()
			if (not _ddcs.erase(old_c)):
				logger.bug("Child mapped to `%s` was missing from the dictionary" % [obj_full_path])


	var grid_sizes:Vector3 = Vector3.ZERO

	# widest row. At most max_children_per row
	grid_sizes.x = max(min(max_children_per_row, data_children.size()), 1)
	# total rows. At most max_rows_per_layer
	grid_sizes.z = max(min(max_rows_per_layer, ceil(data_children.size() / max_children_per_row)), 1)
	# total layers, unbounded
	grid_sizes.y = max(ceil(data_children.size() / max_children_per_row / max_rows_per_layer), 1)


	# usable volume for children
	var children_bb = AABB(
		(Vector3.ONE * -0.5) + children_margin,
		Vector3.ONE - children_margin * 2
	)

	var children_scale:Vector3 = (
			# subtract the spacing to get divisible children volume
			(children_bb.size - children_spacing * (grid_sizes - Vector3.ONE))
		/
			grid_sizes
	)

	# center of the first child, relative to the volume
	var children_base:Vector3 = children_bb.position + children_scale * 0.5

	# offset between centers of the children
	var children_gap:Vector3 = children_spacing + children_scale

	if (children_ar_lock):
		# smallest scale forced as all scales
		children_scale = Vector3.ONE * children_scale[children_scale.min_axis_index()]



	var c_idx:int = -1

	for child_id in data_children:



		c_idx += 1


		var next_c = c_nest.get_node_or_null(child_id)


		if (not next_c):
			if (DEBUG_MODE):
				logger.debug("Creating child `%s/%s/%s`" % [
					get_path(),
					children_holder,
					child_id
				])


			next_c = child_template.instantiate()
			next_c.visible = false

			next_c.name = child_id
			_ddcs[child_id] = next_c

			# Instance() does not call _init, so we do
			# also, it seems to be impossible to override
			# _init with arguments as it ends up resolving
			# to the _init of the native class after that
			next_c.data = data_children[child_id]
			next_c._init()

			c_nest.add_child(next_c)



		# see if the designated translations have changed
		if (next_c is Node3D):

			var child_cell:Vector3 = Vector3.ZERO

			# cell
			child_cell.x = c_idx % max_children_per_row

			# layer
			child_cell.y = c_idx / max_children_per_row / max_rows_per_layer

			# row. Not negated as we fill back to front
			child_cell.z = c_idx / max_children_per_row % max_rows_per_layer


			# Scale down based checked size and align horizontally
			next_c.designated_scale = children_scale

			# Could start pre-scaled but if number of elements changes
			# will cause them to adapt they will
			# Uncomment the following to start pre-scaled
			#next_c.scale = children_scale

			# children are identified by their geometric center,
			# so checked top of putting them in the layer & row they belong
			# we also offset them by half their size

			next_c.designated_translation = (
					# align corner of child to corner of this object
					-Vector3.ONE * 0.5 + (children_scale * 0.5)
				+
					# displace by margin
					children_margin
				+
					# Offset by spacing and indexes
					(children_spacing + children_scale) * child_cell
			)
			
	
			next_c.designated_translation = children_base + children_gap * child_cell

		if (next_c.has_method("update")):
			next_c.update(data_children[child_id])



func _process(delta):

	if (designated_translation != crutch.GDBugVector3INF):

		# if any dimension is still infinity, we need to sanitize that
		var distance:Vector3 = (designated_translation - position)
		if (distance.length() < 0.001):
			_momentum_t = Vector3.ZERO
			designated_translation = crutch.GDBugVector3INF
		else:	
			# Velocity decays by 3% per ms
			_momentum_t *= float(max(1 - (0.03 * sqrt(delta * 1000)), 0))
		
			# Accelerate towards the designated position
			# depending checked how far away that is
			_momentum_t += distance * (0.3 * delta)
			position += _momentum_t

	if (designated_scale != crutch.GDBugVector3INF):

		var distance:Vector3 = (designated_scale - scale)
		if (distance.length() < 0.001):
			_momentum_s = Vector3.ZERO
			designated_scale = crutch.GDBugVector3INF
		else:	
			# Scaling velocity also decays by 3% per ms
			_momentum_s *= float(max(1 - (0.03 * sqrt(delta * 1000)), 0))

			# Same logic as position,
			# just applied to scale
			_momentum_s += distance * (0.3 * delta)
			scale += _momentum_s
