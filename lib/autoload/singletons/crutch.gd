extends Node


# For some reason "Forward" is -z in some coordinate systems.
# We create an forward+righ+up displacement multiplier
const Vector3Positive:Vector3 = (Vector3.FORWARD + Vector3.RIGHT + Vector3.UP)


# WORK AROUNDS FOR GODOT4 Bugs
const GDBugVector2INF:Vector2 = Vector2(INF, INF)
const GDBugVector3INF:Vector3 = Vector3(INF, INF, INF)


# regular expression to validate ISO8601 timestamps
var re_iso:RegEx = RegEx.new()
func _init():

	var c_ok:int = re_iso.compile("^[0-9]{4}(-[0-9]{2}(-[0-9]{2}([ T][0-9]{2}(:[0-9]{2}(:[0-9]{2}(\\.[0-9]+)?)?)?)?)?)?(\\s*(Z|([+-][0-9]{2}(:[0-9]{2})?)))?$")
	assert(c_ok == OK)

func is_iterable(val) -> bool:
	""" Checks if the passed value is an iterable """
	return typeof(val) in [TYPE_ARRAY, TYPE_DICTIONARY]

func dict_path(tree:Dictionary, path:Array, default = null):
	"""
		Resolves a path through the dictionary tree,
		with "path" being an array of keys for each subsequent
		level.
		
		All keys must be strings for this to work
		
		
		Args:
			tree:		(Dictionary)The data tree through which the path is resolved
			path:		(Array)Array of path elements. The empty array returns the dictionary itself
			default:	(any)Value to return if the key is not found

		Returns:
			the resolved dictionary element
			
	"""
	var next_node = tree

	for el in path:
		if (el in next_node):
			next_node = next_node[el]
		else:
			return default

	return next_node

func dict_keys_sort(the_dict:Dictionary) -> Array:
	"""
		Returns the sorted list of a dictionary keys
		
		Args:
			the_dict:		dictionary for which to return sorted keys
			
		Returns:
			the sorted jeys
	"""
	var d_k:Array = the_dict.keys()
	d_k.sort()
	return d_k

func dict_merged_recursive(dict_base:Dictionary, dict_overlay:Dictionary) -> Dictionary:
	"""
		The merged() method in ordinary dictionaries is not recursive. This function is.

		It DOES override entries in A with entries in B UNLESS they're both dictionaries,
		or both arrays. in such case it concatenates them (duplicates are preserved in arrays)

		Args:
			dict_base:		Merge side A
			dict_overlay:		Merge side B
		
		Returns:
			the merger of A and B, recursive
	"""
	var dict_ret:Dictionary = {} # we don't copy recursively here as that might be a waste

	# first, deep copy every base member that doesn't exist in the overlay,
	# as they're final
	for next_key in dict_base:
		if next_key not in dict_overlay:
			if is_iterable(dict_base[next_key]):
				# let recursive duplicate() do its job
				dict_ret[next_key] = dict_base[next_key].duplicate(true)
			else:
				dict_ret[next_key] = dict_base[next_key]


	# scan from overlay now:
	for next_key in dict_overlay:
		var v_base; var t_base:int
		var v_overlay; var t_overlay:int
	
		v_overlay = dict_overlay[next_key]
		t_overlay = typeof(v_overlay)
		
		if (next_key in dict_base):

			v_base = dict_base[next_key]
			t_base = typeof(v_base)

			if (t_overlay == t_base):
				if (t_base == TYPE_ARRAY):
					# concatenate
					dict_ret[next_key] = v_base + v_overlay
					continue
				elif (t_base == TYPE_DICTIONARY):
					# recurse
					dict_ret[next_key] = dict_merged_recursive(v_base, v_overlay)
					continue

		# fallback for all: replace whole value
		dict_ret[next_key] = v_overlay.duplicate(true) if is_iterable(v_overlay) else v_overlay

	return dict_ret
	

func array_enumerate(the_array:Array) -> Dictionary:
	"""
		Analogous to Python's enumerate

		Args:
			the_array:		the array for which to craete an enumeration dictionary

		Returns:
			a dictionary with the array members subscripted by their index

	"""
	var d_out:Dictionary = {}

	for a_idx in range(len(the_array)):
		d_out[a_idx] = the_array[a_idx]

	return d_out


func pprint(data):
	"""
		pretty-prints data
		
		Args:
			data:		the data to be pretty-printed
	"""
	print(JSON.stringify(data, "  ", true))


func iso8601_to_datetime(iso_ts:String) -> Dictionary:
	"""
		Accepts an ISO8601 timestamp and
		converts it into a dictionary like the one returned by OS.get_datetime()
		(which can be passed to Time.get_unix_time_from_datetime_dict())


		Args:
				iso_ts:		the ISO timestamp to process. TZ offsets are supported

		Returns:
				A dictionary much like what OS.get_datetime(true) (it's always UTC)

				A "_second_fraction" subscript is added to the dictionary
				in order to retain sub-second precision if needed (does not 
				seem to interfere with functions that accept `datetime`-type
				dictionaries)

				In case of validation errors, the output dictionary is empty
	"""
	
	if (not re_iso.search(iso_ts)):
		return {}


	var fields:Dictionary = {}
	var offset_abs:int = 0


	# first process the date
	var date_end:int

	fields["year"] = iso_ts.substr(0, 4).to_int()
	date_end = 4
	if ((len(iso_ts) > date_end) and (iso_ts[date_end] == "-")):
		fields["month"] = iso_ts.substr(date_end + 1, 2).to_int()
		date_end = 7

	if ((len(iso_ts) > date_end) and (iso_ts[date_end] == "-")):
		fields["day"] = iso_ts.substr(date_end + 1, 2).to_int()
		date_end = 10


	# Now, with the date out of the way, we can safely split by +/-/Z
	# as the only reason dashes could be in there is a negative TZ offset.
	if (len(iso_ts) > date_end):

		# we skip over what could be either a space or a T (regex alraedy validated that)
		var time_block:String = ""
		var offset_sign:int = 0
		var offset_block:String
		var time_info:String = iso_ts.substr(date_end + 1).strip_edges()
		var t_fields:PackedStringArray


		if ("Z" in time_info):
			t_fields = time_info.split("Z")
			offset_sign = 0
		elif ("-" in time_info):
			t_fields = time_info.split("-")
			offset_sign = -1
		elif ("+" in time_info):
			t_fields = time_info.split("+")
			offset_sign = 1
		else:
			# there is no offset indicator
			offset_sign = 0
			t_fields = [time_info, ""]

		time_block = t_fields[0].strip_edges()
		offset_block = t_fields[1].strip_edges()

		# time to split the time
		t_fields = time_block.split(":")

		# the hour is guaranteed to be there
		fields["hour"] = t_fields[0].to_int()
		if (len(t_fields) > 1):
			fields["minute"] = t_fields[1].to_int()
		if (len(t_fields) > 2):
			fields["second"] = int(floor(t_fields[2].to_float()))
			fields["_second_fraction"] = fmod(t_fields[2].to_float(), 1.0)

		# Tz offset. Note that the offset sign is inverted
		# and indicates the offset of the local time, so we need to reverse it here
		if (offset_sign):
			t_fields = offset_block.split(":")
			offset_abs = int(3600 * t_fields[0].to_int())
			if (len(t_fields) > 1):
				offset_abs += int(60 * t_fields[1].to_int())
			fields["tz_offset"] = offset_sign * -1 * offset_abs


	# populate missing fields
	for m_fld in [
		["month", 1],
		["day", 1],
		["hour", 0],
		["minute", 0],
		["second", 0],
		["_second_fraction", 0.0],
		["tz_offset", 0],
		["dst", false]
	]:
		if (not (m_fld[0] in fields)):
			fields[m_fld[0]] = m_fld[1]


	# Get the timestamp in order to apply the tz offset as linear seconds.
	# Unfortunately Time.get_unix_time_from_datetime_dict() is silly and returns 0 in
	# case of errors, so we need to work around the corner case of the timestamp
	# actually being 1970-01-01 00:00:00

	var u_ts:int
	if (
			(fields["year"] == 1970)
		and
			(fields["month"] == 1)
		and
			(fields["day"] == 1)
		and
			(fields["hour"] == 0)
		and
			(fields["minute"] == 0)
		and
			(fields["second"] == 0)
	):
		u_ts = 0
	else:
		u_ts = Time.get_unix_time_from_datetime_dict(fields)
		if (u_ts == 0):
			# invalid timestamp
			return {}


	var fields_out:Dictionary = Time.get_datetime_dict_from_unix_time(
			u_ts + fields["tz_offset"]
	)
	fields_out["_second_fraction"] = fields["_second_fraction"]
	fields_out["dst"] = false

	return fields_out


func iso8601_to_unix(iso_ts:String) -> float:
	"""
		Wrapper for iso8601_to_datetime() that directly returns the
		UNIX timestamp (as a float, for sub-second precision)

		Arguments:
				iso_ts:		ISO8601 timestamp string

		Returns:
				the unix timestamp as a float, or NaN in case of failure
	"""

	var dt:Dictionary = iso8601_to_datetime(iso_ts)
	if (not dt.size()):
		return NAN

	return float(Time.get_unix_time_from_datetime_dict(dt)) + dt["_second_fraction"]
