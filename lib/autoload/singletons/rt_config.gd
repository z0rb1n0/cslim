class_name RunTimeConfig
extends Node
# Holds the run-time configuration for the system and offers a number
# of support functions


# modes in which _cast_config() can be invoked
enum SettingsIOMode {INPUT = 0, OUTPUT = 1}


const SYSTEM_VERSION:int = 1
const CONFIG_FILE:String = "config.json"

var changes:Mutex = Mutex.new() # claim this before reading/writing config

var settings:Dictionary
var backends:Dictionary
var settings_dynamic:Dictionary # things that are calculated at run-time but must not be saved

const DEFAULT_CONFIG:Dictionary = {
		"version": SYSTEM_VERSION,
		"settings": {
			"controls": {
				"impulse_multiplier": Vector3(0.5, 0.5, 0.5),
				"roll_multiplier": float(1.0),
				"look_sensitivity": Vector2(0.7, 0.7)
			}
		},
		"backends": {
			"local": {
				"host": "localhost",
				"port": 443,
				"connect_timeout": 5.0,
				"username": "cslim",
				"password": "",
				"enabled": true,
				"info": "",
				"verify_ssl": true
			}
		}
}



func _cast_config(s_tree:Dictionary, mode:SettingsIOMode = SettingsIOMode.INPUT):
	"""
		Cast some special sets of settings to/from appropriate godot types.
		
		Specifically, dictionaries that contain Vector2/3 sets of coordinates
		(x, y[, z]) are cast to the correct type in INPUT mode,
		and back to the dictionary in output mode

		Args:
			s_tree:		the settings dictionary
			mode:		whether or not cast should be towards the native types (input) or raw dictionaries (output)

		Returns:
				the cast dictionary
	"""
	var ret:Dictionary
	var s_val

	ret = {}
	for s_key in s_tree:

		s_val = s_tree[s_key]

		# recursion. Unless it's a set of x/y or x/y/z coordinates.
		# In such case we convert into the appropriate vectors

		if (typeof(s_val) == TYPE_DICTIONARY):

			if (
					(s_val.size() == 3)
				and
					s_val.has_all(["x", "y", "z"])
			):

				if (mode == SettingsIOMode.INPUT):
					s_val = Vector3(s_val["x"], s_val["y"], s_val["z"])

			elif (
					(s_val.size() == 2)
				and
					s_val.has_all(["x", "y"])
			):

				if (mode == SettingsIOMode.INPUT):
					s_val = Vector2(s_val["x"], s_val["y"])

			else:

				# any other dictionary causes recursion
				s_val = self._cast_config(s_val, mode)

		else:

			if (typeof(s_val) in [TYPE_VECTOR2, TYPE_VECTOR3]):

				var cast_val:Dictionary = {
					"x": s_val.x,
					"y": s_val.y
				}
				if (typeof(s_val) == TYPE_VECTOR3):
					cast_val["z"] = s_val.z

				s_val = cast_val

		ret[s_key] = s_val

	return ret

func reload():
	var jc_file:FileAccess
	var open_error:Error
	var jc_raw:String

	jc_file = FileAccess.open("user://%s" % CONFIG_FILE, FileAccess.READ)
	open_error = FileAccess.get_open_error()

	#logger.log_level = logger.Priority.DEBUG
	self.changes.lock()

	if (open_error == OK):

		jc_raw = jc_file.get_as_text()
		jc_file.close()

		var json_parser:JSON = JSON.new()
		var parse_error:int = json_parser.parse(jc_raw)
		if parse_error != OK:
			logger.critical("Failed to decodee JSON configuration: error %d at line %d (%s)" % [
				parse_error, json_parser.get_error_line(), json_parser.get_error_message()
			])
			return 5

		var jc_tree:Dictionary = self._cast_config(json_parser.get_data(), SettingsIOMode.INPUT)

		# not sure why people hate indirect attribute references ...
		for data_block in ["settings", "backends"]:
			var db_data:Dictionary
			db_data = crutch.dict_merged_recursive(
				self.DEFAULT_CONFIG[data_block],
				jc_tree.get(data_block, {})
			)
			# some parts of the tree might be read only due to coming from the defaults constant.
			# we cheat and serialise/deserialise the dictionaries
			db_data = str_to_var(var_to_str(db_data))
			self.set(
				data_block,
				db_data
			)

	else:
		# this is bugged at 3.1.1, so we need to make a dangerous assumption
		if (
				(open_error == ERR_FILE_NOT_FOUND)
			or
				(Engine.get_version_info().hex <= 0x030101)
		):
			logger.notice("Configuration file `%s` not found. Loading default configuration and saving it" % CONFIG_FILE)
			self.settings = DEFAULT_CONFIG["settings"]
			self.backends = DEFAULT_CONFIG["backends"]
			if (self.save() != OK):
				logger.error("Failed to save default configuration")


	self.changes.unlock()

	return OK


func save():
	"""
		Saves the settings tree back in the configuration file.
		
		Returns:
			OK checked success, an error code otherwise
	"""
	var jc_file:FileAccess
	var open_error:Error
	var out_config:Dictionary

	jc_file = FileAccess.open("user://%s" % CONFIG_FILE, FileAccess.WRITE)
	open_error = FileAccess.get_open_error()

	if (open_error != OK):
		logger.critical("Unable to open configuration file `%s` for writing (error %d). Cannot proceed" % [CONFIG_FILE, open_error])
		return 4

	out_config = {
		"version": SYSTEM_VERSION,
	}
	
	for data_block:String in ["settings", "backends"]:
		out_config[data_block] = _cast_config(self.get(data_block), SettingsIOMode.OUTPUT)

	self.changes.lock()
	jc_file.store_string(JSON.stringify(out_config, "\t", true))
	self.changes.unlock()


	return OK


func get_setting(path:Array):
	"""
		Retrieves a setting based checked a path array
		
		
		Args:
			setting_path: 	the path in the current configuration from which to retrieve the setting
		
		
		Returns:
			the value of the setting, or null if the setting does not exist
			
	"""
	return crutch.dict_path(self.settings, path)

	
func shutdown(save_config:bool = true):
	if save_config:
		logger.info("Saving configuration...")
		self.save()
	else:
		logger.info("Shutdown was abrupt: not saving configuration")

	logger.info("Shutting down backend connections...")
	shepherd.stop()

	logger.info("Good bye")
	get_tree().quit()


func _init():
	pass
