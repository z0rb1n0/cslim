class_name Logger
extends Node

const RECENT_MESSAGES_MAX = 100 # how many lines to keep in the ring buffer of recent messages

enum Priority {
	CRITICAL,
	BUG,
	WARNING,
	ERROR,
	NOTICE,
	INFO,
	DEBUG
}

# dictionary to translate the level back to its label
const P_LABELS:Dictionary = {
	0: "CRITICAL",
	1: "BUG",
	2: "WARNING",
	3: "ERROR",
	4: "NOTICE",
	5: "INFO",
	6: "DEBUG"
}

var log_level:int = Priority.INFO

var _buf_lock:Mutex = Mutex.new()

var _recent_messages:Array[String] # recent messages
var _rm_pointer:int = -1 # pointer to the most recent element that was added to the array, to avoid big index shifts


func message(msg:Variant, prio:int):

	if ((prio > log_level) or (not (prio in Priority.values()))):
		return 0
	var ts:float = 0.001 * float(Time.get_ticks_msec())

	self._buf_lock.lock() # we claim the lock early to avoid splitting multi-line messages across threads
	for l_line in ("%s" % msg).split("\n", true):

		var msg_line:String = ("%12.03f %-10s %s" % [
			ts,
			"%s:" %P_LABELS[prio],
			l_line
		])

		if (self._rm_pointer + 1) < RECENT_MESSAGES_MAX:
			self._rm_pointer += 1
		else:
			self._rm_pointer = 0

		if self._recent_messages.size() < RECENT_MESSAGES_MAX:
			self._recent_messages.push_back(msg_line)
		else:
			self._recent_messages[self._rm_pointer] = msg_line

			
		print(msg_line)
	self._buf_lock.unlock()

			
	return 0

func critical(msg:Variant):
	return message(msg, Priority.CRITICAL)

func bug(msg:Variant):
	return message(msg, Priority.BUG)

func warning(msg:Variant):
	return message(msg, Priority.WARNING)

func error(msg:Variant):
	return message(msg, Priority.ERROR)

func notice(msg:Variant):
	return message(msg, Priority.NOTICE)

func info(msg:Variant):
	return message(msg, Priority.INFO)

func debug(msg:Variant):
	return message(msg, Priority.DEBUG)

func get_recent_messages(n_msg:int = 5) -> Array[String]:
	"""
		Get the most recent messages from the lookback buffer
		Args:
			n_msg:	the maximum amount of messages to retrieve
		
		Returns:
			an array of messages
	"""
	var ret_arr:Array[String] = []
	self._buf_lock.lock()
	
	# we count on negative array indexes, but limited to available members
	if (self._rm_pointer >= 0): # empty array?
		var ptr:int = self._rm_pointer - min(n_msg, self._recent_messages.size())
		while (ptr <= self._rm_pointer):
			ret_arr.push_back(self._recent_messages[ptr])
			ptr += 1

	self._buf_lock.unlock()

	return ret_arr
