extends Node

const RE_IMAGE_FILES:String = "\\.((PNG)|(JPE?G))$"

const ICONS_PATH:String = "icons" # prefixing this with "res://" issues warnings

const scenes:Dictionary = {
}

var icons:Dictionary = {}
var icons_width = 32 # this controls icon scaling. Aspect ratio will be kept

func _reload():
	"""
		Preloads all static assets in an easily accessible singleton
	"""

	var re_images = RegEx.new()
	re_images.compile(RE_IMAGE_FILES)

	logger.info("[Re]loading icons...")
	var ico_dir:DirAccess

	ico_dir = DirAccess.open(ICONS_PATH)
	if (DirAccess.get_open_error() == OK):
		ico_dir.include_navigational = false
		ico_dir.include_hidden = true
	else:
		logger.warning("Unable to open icons directory")

	if (ico_dir.list_dir_begin()  != OK):
		logger.warning("Unable to open icons for listing")

	while (true):
		var next_file = ico_dir.get_next()
		if (not ((typeof(next_file) == TYPE_STRING) and len(next_file))):
			break
		if (not re_images.search(next_file.to_upper())):
			continue

		var ico_index:String = re_images.sub(next_file.to_upper(), "").to_lower()
		logger.info("..`%s` as icon `%s`" % [next_file, ico_index])


		var ico_img:Image = Image.new()
		var ico_path:String = "%s/%s" % [ICONS_PATH, next_file]
		if (ico_img.load(ico_path) != OK):
			logger.error("Unable to open icon at `%s`" % [ico_path])
		ico_img.resize(
			icons_width,
			int(round(float(icons_width) / (float(ico_img.get_size().x) / float(ico_img.get_size().y)))),
			Image.INTERPOLATE_CUBIC
		)

		var ico_tx:ImageTexture = ImageTexture.create_from_image(ico_img)

		icons[ico_index] = ico_tx

func _ready():
	self._reload()
