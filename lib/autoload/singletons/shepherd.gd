class_name BackendShepherd
extends Node
# Shepherd looking after the flock of backends

const HERDING_INTERVAL = 0.1

# the backends of connected backends.
# Each member - subscripted by backend Id - contains
# the CSLIMBackend object
var backends:Dictionary = {}

var _thread:Thread = Thread.new()


# This dictionary holds whatever was popped from the inbox
# of each backend. The message format is the same
# as the one in CSLIMBackend, with an additional "backend"
# subscript to identify which backend it came from
var _inbox:Array = []



func _block_set(_value):
	push_error("Read-only property")


var _lock:Mutex = Mutex.new()
var lock:Mutex:
	get:
		return _lock
	set(value):
		return _block_set(value)

var _active:bool = false
var active:bool :
	get:
		return _active
	set(value):
		return _set_activation_state(value)


# Set this to some function reference and that function
# will be called at every update
var report_callback:Callable


func _set_activation_state(target_state:bool):
	if (target_state):
		start()
	else:
		stop()


func start():
	if (not active):
		logger.info("Backend shepherd activating. The main loop will be initialized and backends will be activated")
		self._active = true
		#if (_thread.start(Callable(self, "_main_loop")) == OK):
		if (_thread.start(_main_loop.bind()) == OK):
			_lock.lock()
			for backend_id in backends:
				if (not backends[backend_id].active):
					backends[backend_id].start()
			_lock.unlock()
		else:
			self._active = false
			logger.error("Could not start backend shepherd loop")

func stop():
	if (active):
		logger.debug("Stopping backend shepherd. Waiting for all backends to shut down")
		self._lock.lock()
		for backend_id in backends:
			if (backends[backend_id].active):
				backends[backend_id].stop()
		self._lock.unlock()
		self._active = false
		self._thread.wait_to_finish()


func update_backends():
	"""
		[re]initialises & starts the backend objects in the backends
		based checked the configuration.
	"""
	_lock.lock()

	# first scan disables the backends that no longer exist
	for backend_id in backends.duplicate():

		var remove_backend:bool = false

		if (not rt_config.settings["backends"].get(backend_id, {}).get("enabled", false)):
			logger.info("Backend `%s` was removed from the configuration or disabled. Removing from backends" % [
				backend_id
			])
			
			remove_backend = true

		else:

			var be_info:Dictionary = rt_config.settings["backends"][backend_id]
			var be_hash:String = CSLIMBackend.hash_config(
				be_info["host"],
				be_info["port"],
				be_info["connect_timeout"],
				CSLIMBackend.BackendMode.MODE_DIRECT,
				be_info["username"],
				be_info["password"],
				false
			)

			if (backends[backend_id].conf_hash != be_hash):
				logger.info("Backend `%s` settings have changed. Removing from backends (the updated one will be re-added later)" % [
					backend_id
				])
				remove_backend = true

		if (remove_backend):
			backends[backend_id].stop()
			if (not backends.erase(backend_id)):
				logger.error("Backend `%s` not found in the backends as expected" % [backend_id])

	# second scan adds what's missing
	for backend_id in rt_config.backends:

		var be_info:Dictionary = rt_config.backends[backend_id]

		if ((backend_id in backends) or (not be_info.get("enabled", false))):
			# already there or disabled?
			continue


		# this must be created:
		logger.info("Backend `%s` is defined & enabled but not present in the backends. Adding" % [
			backend_id
		])


		self.backends[backend_id] = CSLIMBackend.new(
			be_info["host"],
			be_info["port"],
			be_info["connect_timeout"],
			CSLIMBackend.BackendMode.MODE_DIRECT,
			be_info["username"],
			be_info["password"],
			be_info["verify_ssl"]
		)
		if (active):
			backends[backend_id].start()

	_lock.unlock()


func _main_loop():

	while (active):

		_lock.lock()
		for backend_id in backends:
			var be_msgs:Array = backends[backend_id].pop()
			for msg in be_msgs:
				msg["backend_id"] = backend_id
				_inbox.append(msg)

		# update status in the UI if that is configured
		if (report_callback.is_valid()):
			report_callback = get_backends_statuses

		_lock.unlock()

		OS.delay_usec(int(round(1000000.0 * HERDING_INTERVAL)))

func pop():
	"""
		Pops the inbox array and re-references it
	"""
	self._lock.lock()
	var msgs_out:Array = _inbox
	_inbox = []
	self._lock.unlock()
	return msgs_out


func send(backend_id:String, message:PackedByteArray) -> int:
	"""
		Wrapper for a given backend's "send" functionality.

		Args:
			backend:	the backend name
			message:	the message

		Returns:
			an error level (or 0)
	"""
	self._lock.lock()
	var i_out:int
	if (backend_id in backends):
		i_out = backends[backend_id].send(message)
	else:
		i_out = ERR_UNAVAILABLE
	self._lock.unlock()

	return i_out


func send_all(message:PackedByteArray) -> Dictionary:
	"""
		Wrapper for "send" that attempts to send a message to
		all backends.

		Args:
			backend:	the backend name
			message:	the message

		Returns:
			ditionary of returned errors, indexed by backend name
	"""
	_lock.lock()
	var bes:Array = backends.keys().duplicate()
	_lock.unlock()
	var d_out:Dictionary = {}
	for backend_id in bes:
		d_out[backend_id] = send(backend_id, message)
	return d_out


func get_backends_statuses() -> Dictionary:
	"""
		Returns a dictionary, indexed by backends, with the statuses
		as reported by individual backends.

		The whole operation is based checked scalars, so it does not
		need locks

		Returns:
			a dictionary subscripted by backend id. Each member contains
			a 3-list:

			0) The output of CSLIMBakend.get_state() for this backend
			1) The last error number the backend reported
			2) A description for the state (hard coded here)

	"""
	var d_states:Dictionary = {}
	for backend_id in backends:
		var fields:Array = [
			backends[backend_id].status,
			backends[backend_id].last_error
		]

		if (fields[0] == CSLIMBackend.BackendStatus.STATUS_INACTIVE):
			fields.append("Inactive")
		elif (fields[0] == CSLIMBackend.BackendStatus.STATUS_CONNECTING):
			fields.append("Connecting")
		elif (fields[0] == CSLIMBackend.BackendStatus.STATUS_CONNECTED):
			fields.append("Connected")
		elif (fields[0] == CSLIMBackend.BackendStatus.STATUS_ERROR):
			fields.append("Error (%d)" % fields[1])
		else:
			fields.append("UNKNOWN!!!") # weird

		d_states[backend_id] = fields

	return d_states
