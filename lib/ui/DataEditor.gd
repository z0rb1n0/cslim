class_name DataEditor
extends Window

# Accepts  a `schema` dictionary and lays out a data editor form.
#
# See open() for schema definition specifics
#
# Upon confirmation, passes the current object type, id and dictionary to the supplied callback
#
# TODO: make hierarchical through recursion

var object_type:String = ""
var object_id:String = ""
var schema:Dictionary = {} # this is stored post-sanitisation; also contains object refs
var handler:Callable
var modal:bool = false

var invalid_attributes:Array = []


func _init():

	self.name = "data_editor"

	# main areas group
	var c_areas:VBoxContainer = VBoxContainer.new()
	c_areas.name = "areas"
	self.add_child(c_areas, true)
	
	# main containers for data & buttons
	var c_data:ScrollContainer = ScrollContainer.new()
	c_data.name = "data"
	c_areas.add_child(c_data, true)

	var c_buttons:HBoxContainer = HBoxContainer.new()
	c_buttons.name = "buttons"
	c_areas.add_child(c_buttons, true)

	# buttons block, includes separators
	var c_reset:Button = Button.new()
	c_reset.name = "reset"
	c_reset.text = "Reset"
	c_buttons.add_child(c_reset, true)

	if (c_reset.connect("pressed",Callable(self,"reset_data")) != OK):
		push_error("Couldn't connect reset button")

	var c_s0:Control = Control.new()
	c_s0.name = "s0"
	c_buttons.add_child(c_s0, true)

	var c_submit:Button = Button.new()
	c_submit.name = "submit"
	c_submit.text = "Submit"
	c_buttons.add_child(c_submit, true)

	if (c_submit.connect("pressed", Callable(self,"_submit")) != OK):
		push_error("Couldn't connect submit button")

	# Maximise all things
	for c_max in [
		self, c_areas, c_data, c_buttons,
		c_reset, c_s0, c_submit
	]:
		for att_t in ["anchor", "margin"]:
			for att_s in [
				["left", 0.0],
				["right", 1.0],
				["top", 0.0],
				["bottom", 1.0]
			]:
				c_max.set("%s_%s" % [att_t, att_s[0]], att_s[1])

		for att_s in ["horizontal", "vertical"]:
			# Everything is expand & fill
			c_max.set("size_flags_%s" % [att_s], Control.SIZE_FILL | Control.SIZE_EXPAND)

		# sizing is a bit specific in some cases
		#if (c_max == c_data):
			#c_max.size_flags_stretch_ratio = 0.9
		#elif (c_max == c_buttons):
			#c_max.size_flags_stretch_ratio = 0.1
		#else:
			#c_max.size_flags_stretch_ratio = 1.0

	# close buttton

	if (connect("close_requested", queue_free.bind()) != OK):
		logger.bug("Unable to connect backends tree buttons to handler")


func _validate_attribute(new_value, attribute:Array):
	"""
		Given the name of a data attribute (or path when recursion will be implemented),
		validates the contents of its control against the constraints in the schema,
		sets it colour/tooltip accordingly and enables/disables the submit button.

		Args:
			new_value:		(any)The new value in the input
			attribute:		(Dictionary)Path3D elements of the attribute. Until reursion is
							implemented, it will only contain a single value
	"""
	var att_key:String = String(attribute[0])
	var att_def:Dictionary = self.schema.get(att_key, null)


	if (typeof(att_def) != TYPE_DICTIONARY):
		push_error("Invalid/missing definition for attribute \"%s\"" % [att_key])

	var err_str:String = ""
	# only strings need validation as numeric input is checked-rails
	if (att_def["_type_internal"] in [TYPE_BOOL]):
		pass
	elif (att_def["_type_internal"] in [TYPE_INT, TYPE_FLOAT]):
		# att the moment this handler is only invoked for LineInput so this flow is never used
		#new_value = int(round(att_def["_control_value"].value)) if (att_def["_type_internal"] == TYPE_INT) else att_def["_control_value"].value
		pass
	elif (att_def["_type_internal"] == TYPE_STRING):
		if (("min_length" in att_def["modifiers"]) and (len(new_value) < att_def["modifiers"]["min_length"])):
			err_str = "Must be at least %d characters long" % [att_def["modifiers"]["min_length"]]
		elif (("max_length" in att_def["modifiers"]) and (len(new_value) > att_def["modifiers"]["max_length"])):
			err_str = "Must be at most %d characters long" % [att_def["modifiers"]["max_length"]]
		elif (("_regex_compiled" in att_def["modifiers"]) and (not att_def["modifiers"]["_regex_compiled"].search(new_value))):
			err_str = "Must match regular expression %s" % [att_def["modifiers"]["_regex_compiled"].get_pattern()]
	else:
		push_error("Invalid type in processed editor schema (key: `%s`)" % [att_key])

	att_def["_control_status"].tooltip_text = err_str
	if (len(err_str)):
		att_def["_control_status"].texture = assets.icons["rejected"]
		self.invalid_attributes.append(att_key)
	else:
		att_def["_control_status"].texture = assets.icons["accepted"]
		self.invalid_attributes.erase(att_key)

	$areas/buttons/submit.disabled = bool(len(self.invalid_attributes))
	$areas/buttons/submit.tooltip_text = "Some of the data is invalid" if (bool(len(self.invalid_attributes))) else ""


func _layout(schema:Dictionary, attach_to:Control):
	"""
		Processed the supplied schema and generates the internal
		sanitised copy (with references to the objects)
		
		Args:
			schema:			the schema to load
			attach_to:		the control to which the input container will be attached.
							(Future-proofing for recursion)
	"""

	var atts_node:VBoxContainer = VBoxContainer.new()
	atts_node.name = "attributes"
	atts_node.size_flags_horizontal = Control.SIZE_FILL | Control.SIZE_EXPAND
	atts_node.size_flags_vertical =Control.SIZE_FILL

	for att_key in schema:

		var att_def:Dictionary = schema[att_key]


		# if the type is not specified, "string" is assumed
		var att_type:String = att_def.get("type", "string")
		var c_value:Control
		var att_modifiers:Dictionary = att_def.get("modifiers", {})

		var att_modifiers_processed:Dictionary = {}

		var att_internal_type = TYPE_NIL # designated type of the value
		var att_default = null # a sanitised value for the default

		if (att_type in ["bool", "boolean"]):
			att_internal_type = TYPE_BOOL
		elif (att_type in ["int", "integer"]):
			att_internal_type = TYPE_INT
		elif (att_type in ["float", "real"]):
			att_internal_type = TYPE_FLOAT
		elif (att_type in ["string"]):
			att_internal_type = TYPE_STRING
		else:
			push_error("Unsupported type `%s` in schema" % [att_type])

		# readonly attributes get a read-only inputbox anyway
		var att_writable:bool = bool(att_def.get("writable", false))

		if (att_internal_type in [TYPE_BOOL]):

			c_value = CheckBox.new()
			c_value.disabled = (not att_writable)
			att_default = bool(att_def.get("default") or false)

		elif (att_internal_type in [TYPE_INT, TYPE_FLOAT]):

			c_value = SpinBox.new()
			c_value.editable = att_writable
			c_value.rounded = (att_internal_type == TYPE_INT)


			for threshold in ["min_value", "max_value"]:

				if (threshold in att_modifiers):
					# forced sanitation & rounding
					if (att_internal_type == TYPE_INT):
						att_modifiers_processed[threshold] = int(round(float(att_modifiers[threshold])))
					elif (att_internal_type == TYPE_FLOAT):
						att_modifiers_processed[threshold] = float(att_modifiers[threshold])
					else:
						push_error("Invalid value for `%s` modifier for field `%s`" % [att_key, threshold])

					c_value.set(threshold, float(att_modifiers_processed[threshold]))

			if ("default" in att_def):

				var default_type:int = typeof(att_def.get("default", null))

				if (default_type == TYPE_NIL):
					att_default = null
				elif (default_type == TYPE_INT):
					att_default = int(att_def["default"])
				elif (default_type == TYPE_FLOAT):
					att_default = float(att_def["default"])
				else:
					push_error("Invalid data editor default `%s` for type `%s`(%s) in object `%s`(%s)" % [
						att_def.get("default", null),
						att_type,
						att_key,
						self.object_type,
						self.object_id
					])

		elif (att_type == "string"):

			# we need this early
			att_modifiers_processed["long"] = att_modifiers.get("long", false)

			# length limits are always expressed in the processed schema when supported
			if ("min_length" in att_modifiers):
				if (att_modifiers_processed["long"]):
					push_error("attribute `%s`: min_length is not supported for long strings" % [att_key])
				else:
					var min_length = att_modifiers["min_length"]
					if ((typeof(min_length) != TYPE_INT) or (min_length < 0)):
						push_error("Invalid \"min_length\" modifier for attribute \"%s\"" % [att_key])
					att_modifiers_processed["min_length"] = min_length

			if ("max_length" in att_modifiers):
				if (att_modifiers_processed["long"]):
					push_error("attribute `%s`: max_length is not supported for long strings" % [att_key])
				else:
					var max_length = att_modifiers["max_length"]
					if ((typeof(max_length) != TYPE_INT) or (max_length > 0x0fffffff)):
						push_error("Invalid \"max_length\" modifier for attribute \"%s\"" % [att_key])
					att_modifiers_processed["max_length"] = max_length

			# regex
			if ("regex" in att_modifiers):
				if (att_modifiers_processed["long"]):
					push_error("attribute `%s`: regex is not supported for long strings" % [att_key])
				else:
					var att_regex = att_modifiers["regex"]
					if (typeof(att_regex) != TYPE_NIL):
						var re_att:RegEx = RegEx.new()
						if (re_att.compile(String(att_regex)) != OK):
							push_error("Invalid regular expression for attribute \"%s\"" % [att_key])
						att_modifiers_processed["_regex_compiled"] = re_att

			if (att_modifiers_processed["long"]):
				c_value = TextEdit.new()
				c_value.editable = att_writable
			else:
				c_value = LineEdit.new()
				c_value.editable = att_writable
				if ("max_length" in att_modifiers_processed):
					c_value.max_length = att_modifiers_processed["max_length"]
				c_value.secret = (att_modifiers.get("secret", false))

			# for strings we can liberally cast
			att_default = att_def.get("default")
			# We check for nulls as a second stage as they could literally be in the schema
			if (typeof(att_default) == TYPE_NIL):
				att_default = ""
			else:
				att_default = String(att_default)

		# connect for validation. Not supported/needed for numerics and long text
		if ((att_internal_type == TYPE_STRING) and (not att_modifiers_processed.get("long", false))):
			if (c_value.connect("text_changed",Callable(self,"_validate_attribute").bind(att_key)) != OK):
				push_error("Could not connect control to validation routine")


		# create the controls for this attribute row
		var c_row:HBoxContainer = HBoxContainer.new()
		c_row.name = att_key
		c_row.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		c_row.size_flags_vertical = Control.SIZE_FILL

		var c_label:Label = Label.new()
		c_label.name = "label"
		c_label.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		c_label.size_flags_vertical = Control.SIZE_EXPAND_FILL
		c_label.size_flags_stretch_ratio = 0.25
		c_label.text = att_def.get("label", "Attribute `%s`" % [att_key]) + ":"
		c_row.add_child(c_label, true)


		c_value.name = "value"
		c_value.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		c_value.size_flags_vertical = Control.SIZE_EXPAND_FILL
		c_value.size_flags_stretch_ratio = 0.7
		c_row.add_child(c_value, true)


		var c_status:TextureRect = TextureRect.new()
		c_value.name = "status"
		c_status.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		c_status.size_flags_vertical = Control.SIZE_EXPAND_FILL
		c_status.size_flags_stretch_ratio = 0.05
		c_status.texture = assets.icons["accepted"]
		c_status.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
		c_row.add_child(c_status, true)

		atts_node.add_child(c_row, true)


		# update the processed schema. We do it this late 'cause we need
		# a couple of the above controls

		self.schema[att_key] = {
			"type": att_type,
			"modifiers": att_modifiers_processed,
			"default": att_default,
			"writable": att_writable,
			"_type_internal": att_internal_type,
			"_control_value": c_value,
			"_control_status": c_status
		}
		

	attach_to.add_child(atts_node, true)


func reset_data():
	""" Resets the form fields their original values """
	for att_key in self.schema:

		var att_def:Dictionary = schema[att_key]
	
		var target_att:String
		var set_value = att_def["default"]

		if (typeof(set_value) == TYPE_NIL):
			# value has no default. Cannot reset
			continue


		# different control types force us to use different attributes
		if (att_def["_type_internal"] in [TYPE_BOOL]):
			target_att = "pressed"
			set_value = bool(set_value)
		elif (att_def["_type_internal"] in [TYPE_INT, TYPE_FLOAT]):
			target_att = "value"
			set_value = float(set_value)
		elif (att_def["_type_internal"] in [TYPE_STRING]):
			target_att = "text"
			set_value = String(set_value)
		else:
			push_error("Invalid type in processed editor schema (key: `%s`)" % [att_key])

		att_def["_control_value"].set(target_att, set_value)

		# setting the text/value does not trigger the validatin
		self._validate_attribute(set_value, [att_key])



func collect_data() -> Dictionary:
	"""
		Returns the data the form is currently holding as a dictionary
		
		
		Returns:
				(dictionary)the data dictionary with the contents of the form,
				with values properly cast. Does NOT perform any validation
				of the current contents
	"""

	var d_data:Dictionary = {}

	for att_key in self.schema:

		var att_def:Dictionary = self.schema[att_key]
		if (att_def["_type_internal"] in [TYPE_BOOL]):

			d_data[att_key] = att_def["_control_value"].pressed

		elif (att_def["_type_internal"] in [TYPE_INT, TYPE_FLOAT]):

			d_data[att_key] = att_def["_control_value"].value

			if (att_def["_type_internal"] == TYPE_INT):
				d_data[att_key] = int(round(d_data[att_key]))

		elif (att_def["_type_internal"] == TYPE_STRING):

			d_data[att_key] = att_def["_control_value"].text

		else:
			push_error("Unsupported internal type stored in the processed schema: %d" % [att_def["_type_internal"]])

	return d_data

func _submit():

	self.handler.call(
		self.object_type,
		self.object_id,
		self.collect_data()
	)
	self.queue_free()

func _input(ev):

	# in modal mode, close checked the menu "escape"
	if (self.modal):
		if (ev.is_action("ui_escape") or ev.is_action("ui_accept")):
			get_viewport().set_input_as_handled()

			if (ev.is_action("ui_accept")):
				if (not $areas/buttons/submit.disabled):
					$areas/buttons/submit.emit_signal("pressed")
			else:
				self.queue_free()


func open(
		parent_control:Control,
		object_type:String,
		object_id:String,
		schema:Dictionary,
		handler:Callable,
		modal:bool = false
	):
	"""
		Generates and opens the editing dialog window.


		Args:
			parent_control:			note to which the window will be attached
			object_type:			type of the object being edited
			object_id:				identifier of the object being edited
			schema:					schema for the form (and possible default values if editing)
									See SCHEMA DEFINITION for details
			handler:				function to which the data contained in the form
									will be passed when `Submit` is presed.
									The callback function is expected to accept 3 arguments:
										- a string containing the type of object being edited (can be empty)
										- a string containing the identifier of object being edited (empty for new objects)
										- the dictionary containing the structured data, cast according to the schema
			modal:					Whether or not the window should be modal


		SCHEMA DEFINITION

		The dictionary members define an input field (key being the field name)
		
		Structure of a field definition (one per dictionary subscript)
		{
			'label':			(string) label that will be shown
			'type':				(string) type of field. Can be int, string or float
			'writable':			(boolean)Whether or not the field is to be editable
			'modifiers':		(dictionary)Modifiers for the given type, many of which are type-specific
				'min_value':	(float/int)Minimum value. Only applicable to numeric types.
				'max_value':	(float/int)Maximum value. Only applicable to numeric types.
				'secret':		(boolean)Only applicable to strings. Causes the input to be masked (eg: passwords)
				'min_length':	(int)Minimum length. Only applicable to string types.
				'max_length':	(int)Maximum length. Only applicable to string types.
				'regex':		(string)Validation regular expression, only applicable to strings
				'long':			(bool)Valid for strings only: uses a TextEdit as opposed to LineEdit
			'default':		(any)Default value for the field. The function will ignore it
							and log an error if it's not type-compatible with the type
		}
	"""

	self.object_type = object_type
	self.object_id = object_id
	self.handler = handler
	self.modal = modal

	if (self.object_id):
		self.window_title = "Editing `%s`: `%s`" % [
			object_type, object_id
		]
	else:
		self.title = "Editing new `%s`" % [object_type]

	self._layout(schema, $areas/data)
	self.reset_data()

	parent_control.add_child(self, true)

	self.exclusive = self.modal
	self.popup_centered_ratio(0.4)
