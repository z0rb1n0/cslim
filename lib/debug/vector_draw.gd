extends Control


var _vectors:Array[Array]

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

func _draw_vector(origin:Vector3, vec:Vector3):
	var cam:Camera3D = self.get_viewport().get_camera_3d()
	var start = cam.unproject_position(origin)
	var end = cam.unproject_position(origin + vec)
	#self.draw_line(start, end, Color.GREEN, 3)
	self.draw_line(Vector2(0, 0), Vector2(1, 1), Color.GREEN, 3)

func _draw_all_vectors():
	for v_i:Array in self._vectors:
		self._draw_vector(v_i[0], v_i[1])

func add_vector(origin:Vector3, vec:Vector3):
	self._vectors.append([origin, vec])

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(_delta: float) -> void:
	#self.queue_redraw()

#func _draw():
	#_draw_all_vectors()
	#self._draw_all_vectors()
	#self._vectors.clear()
