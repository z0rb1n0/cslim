class_name CSLIMUniverse
extends Node3D

const ROOT_NODE_ORIGIN = Vector3(0, 12, 0)
const ROOT_NODE_SCALE = Vector3(20, 20, 20)


@onready var root_scene:PackedScene = load("res://entities/network/network.tscn")

# data-driven nodes root
var ddn_root:Node3D

func _enter_tree():
	pass


func _ready():

	logger.log_level = logger.Priority.DEBUG if (OS.is_debug_build()) else logger.Priority.INFO

	if (rt_config.reload() != OK):
		self.queue_free() # without this, some _ready() seems to fire after get_tree().quit()
		rt_config.shutdown(false) # ouch

	ddn_root = root_scene.instantiate()
	ddn_root.name = "the_net"
	# Overriding _init with extra arguments seems to
	# target the _init of the base class (spatial) instead,so we need to
	# push the dataset in as an extra step
	ddn_root._init()
	add_child(ddn_root)
	ddn_root.designated_translation = ROOT_NODE_ORIGIN
	ddn_root.scale = ROOT_NODE_SCALE


	shepherd.update_backends()
	shepherd.start()


func _process(delta):

	# compiler complains if we don't do this and I don't want to prefix builtin arguments with _
	if (delta < 0.0):
		pass

	if (not is_instance_valid(ddn_root)):
		print("Root node not ready (%s)" % ddn_root)
		return null
